
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:support/page/exercice/exercice_selection_page.dart';
import 'package:support/page/feeling/feeling_page.dart';
import 'package:support/page/home/home_page.dart';
import 'package:support/page/login/login_page.dart';
import 'package:support/page/post/create_post_page.dart';
import 'package:support/page/post/post_details_page.dart';
import 'package:support/page/profile/update_profile_page.dart';
import 'package:support/page/register/register_page.dart';
import 'package:support/page/welcome/welcome_page.dart';
import 'package:support/providers/auth_provider.dart';
import 'package:support/page/home/home_page.dart';

final routes = {
  '/': (context) => Router(),

  //----------------------------- AUTH ROUTES-------------------------

  LoginPage.routeName: (BuildContext context) => LoginPage(),
  RegisterPage.routeName: (BuildContext context) => RegisterPage(),
  HomePage.routeName: (BuildContext context) => HomePage(),
  FeelingPage.routeName: (BuildContext context) => FeelingPage(),
  PostDetaisPage.routeName : (BuildContext context) => PostDetaisPage(),
  ExerciceSelectionPage.routeName : (BuildContext context) => ExerciceSelectionPage(),
  UpdateProfilePage.routeName : (BuildContext context) => UpdateProfilePage(),

  //------------------------- END AUTH ROUTES-------------------------


  //----------------------------- USER ROUTES-------------------------

  //------------------------- END AUTH ROUTES-------------------------



};

class Router extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<AuthProvider>(
      builder: (context, auth, child) {
        switch (auth.status) {
          case Status.Unauthenticated:
            return WelcomePage();
          case Status.Authenticated:
            return HomePage();


            default:
            return WelcomePage();
        }
      },
    );
  }
}
