// ignore_for_file: unnecessary_const, prefer_const_constructors

import 'package:flutter/material.dart';

class CommentStyle {
  static const Color fogytex = Color(0xffA9A9A9);
  static const Color hintTextColor = Color(0xffCCCCCC);
  static const Color homeBackgroundColor = Color(0xFFD8F1DF);

  static const MaterialColor myThemeColor = const MaterialColor(
    0xff33BC59,
    const <int, Color>{
      50: const Color(0xFFe0e0e0),
      100: const Color(0xFFb3b3b3),
      200: const Color(0xFF808080),
      300: const Color(0xFF4d4d4d),
      400: const Color(0xFF262626),
      500: const Color(0xff33BC59),
      600: const Color(0xFF000000),
      700: const Color(0xFF000000),
      800: const Color(0xFF000000),
      900: const Color(0xFF000000),
    },
  );

  static String getTextFromDate(String date){
    String datestring  = '';
    datestring = date.substring(0,10)+' '+date.substring(12,16);
    return datestring;
  }
  static RichText getNotText(String user,String content){

    return new RichText(
      text: new TextSpan(
        // Note: Styles for TextSpans must be explicitly defined.
        // Child text spans will inherit styles from parent
        style: new TextStyle(
          fontSize: 14.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: user+' ',style: smallBoldBlackTxtStyle()),
          new TextSpan(text: content, ),
        ],
      ),
    );
  }

  static textFieldDecoration(hint) {
    return InputDecoration(
      hintText: hint,
      floatingLabelBehavior: FloatingLabelBehavior.auto,
      hintStyle: TextStyle(
          color: CommentStyle.hintTextColor,
          fontSize: 15,
          fontFamily: 'Poppins'),
    );
  }

  static MediumBoldBlackTxtStyle() {
    return TextStyle(
      fontSize: 18,
      fontFamily: 'Poppins',
      fontWeight: FontWeight.w700,
    );
  }

  static commmentTextStyle() {
    return TextStyle(
        fontSize: 10, fontFamily: 'Poppins', color: Color(0xFF545454));
  }

  static commentTextFieldStyle() {
    return InputDecoration(
      hintText: 'Add a comment..',
      filled: true,
      contentPadding: const EdgeInsets.only(left: 20),
      fillColor: Color(0xFFFFFFFF),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
        borderRadius: BorderRadius.circular(18),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(18),
        borderSide: BorderSide(color: Color(0xFFF2F2F2)),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5),
        borderSide: BorderSide(color: Color(0xFFF2F2F2)),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5),
        borderSide: BorderSide(color: Color(0xFFF2F2F2)),
      ),
    );
  }

  static smallBoldBlackTxtStyle() {
    return TextStyle(
      fontSize: 12,
      fontFamily: 'Poppins',
      fontWeight: FontWeight.w700,
    );
  }

  static smallBoldWhiteTxtStyle() {
    return TextStyle(
      fontSize: 14,
      color: Colors.white,
      fontFamily: 'Poppins',
      fontWeight: FontWeight.w700,
    );
  }

  static smallWhiteTxtStyle() {
    return TextStyle(
      fontSize: 8,
      color: Colors.white,
      fontFamily: 'Poppins',
      fontWeight: FontWeight.w700,
    );
  }
  static largeWhiteTxtStyle() {
    return TextStyle(
      fontSize: 35,
      color: Colors.white,
      fontFamily: 'Poppins',
      fontWeight: FontWeight.w700,
    );
  }
  static largBoldGreenTxtStyle() {
    return TextStyle(
        fontFamily: 'Poppins',
        fontSize: 25,
        fontWeight: FontWeight.bold,
        color: myThemeColor);
  }
  static mediumBoldGreenTxtStyle() {
    return TextStyle(
        fontFamily: 'Poppins',
        fontSize: 15,
        fontWeight: FontWeight.bold,
        color: myThemeColor);
  }

  static smallBoldGreenTxtStyle() {
    return TextStyle(
        fontFamily: 'Poppins',
        fontSize: 15,
        fontWeight: FontWeight.w500,
        color: myThemeColor);
  }

  static small13BoldGreenTxtStyle() {
    return TextStyle(
        fontFamily: 'Poppins',
        fontSize: 13,
        fontWeight: FontWeight.w500,
        color: Color(0xFF545454));
  }

  static smallFogyTxtStyle() {
    return TextStyle(
        fontFamily: 'Poppins', fontSize: 12, color: CommentStyle.fogytex);
  }

  static smallBoldFogyTxtStyle() {
    return TextStyle(
        fontFamily: 'Poppins',
        fontSize: 10,
        color: CommentStyle.fogytex,
        fontWeight: FontWeight.w700);
  }

  static extraSmallFogyTxtStyle() {
    return TextStyle(
        fontFamily: 'Poppins', fontSize: 8, color: CommentStyle.fogytex);
  }

  static smallTextStyle() {
    return TextStyle(
      fontSize: 12,
      fontFamily: 'Poppins',
    );
  }

  static tabItemTxtStyle() {
    return TextStyle(
      fontFamily: 'Poppins',
      fontSize: 12,
      color: Color(0xFFA9A9A9),
    );
  }

  static addPostTextField(string) {
    return InputDecoration(
      hintText: string,
      hintStyle: smallBoldFogyTxtStyle(),
      filled: true,
      fillColor: Colors.white,
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(40),
        borderSide: BorderSide(color: Colors.white),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(40),
        borderSide: BorderSide(color: Colors.white),
      ),
    );
  }

  static searshTextField() {
    return InputDecoration(
      hintText: 'Searsh...',
      hintStyle: CommentStyle.smallFogyTxtStyle(),
      filled: true,
      contentPadding: const EdgeInsets.all(5),
      fillColor: Color(0xFFF2F2F2),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(40),
        borderSide: BorderSide(color: Color(0xFFF2F2F2)),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(40),
        borderSide: BorderSide(color: Color(0xFFF2F2F2)),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(40),
        borderSide: BorderSide(color: Color(0xFFF2F2F2)),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(40),
        borderSide: BorderSide(color: Color(0xFFF2F2F2)),
      ),
      prefixIcon: Icon(Icons.search),
    );
  }


  static feelingTextField() {
    return InputDecoration(
      hintText: 'How you are feeling ?',
      hintStyle: CommentStyle.smallFogyTxtStyle(),
      filled: true,
      contentPadding: const EdgeInsets.all(5),
      fillColor: Color(0xFFF2F2F2),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(40),
        borderSide: BorderSide(color: Color(0xFFF2F2F2)),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(40),
        borderSide: BorderSide(color: Color(0xFFF2F2F2)),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(40),
        borderSide: BorderSide(color: Color(0xFFF2F2F2)),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(40),
        borderSide: BorderSide(color: Color(0xFFF2F2F2)),
      ),

    );
  }

  static smallGreenTxtStyle() {
    return TextStyle(
      fontSize: 10,
      decoration: TextDecoration.underline,
      fontFamily: 'Poppins',
      fontWeight: FontWeight.w500,
      color: CommentStyle.myThemeColor,
    );
  }
}
