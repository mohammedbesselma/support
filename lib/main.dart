import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:support/model/comment.dart';
import 'package:support/model/feelingcategorie.dart';
import 'package:support/model/like.dart';
import 'package:support/model/notification.dart';
import 'package:support/model/post.dart';
import 'package:support/model/saving.dart';
import 'package:support/model/tip.dart';
import 'package:support/model/user.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:support/providers/auth_provider.dart';
import 'package:support/services/local_notification_service.dart';
import 'package:support/utilities/comment_style.dart';
import 'package:support/utilities/routes.dart';

Future<void> backgroundHandler(RemoteMessage message) async{
  print(message.data.toString());
  print(message.notification!.title);
}

Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(backgroundHandler);

  runApp(MyApp());

}

class MyApp extends StatefulWidget {






  static GlobalKey<NavigatorState> navigatorKey =
  GlobalKey(debugLabel: "Main Navigator");

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {


  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: AuthProvider()),
        ChangeNotifierProvider.value(value: User()),
        ChangeNotifierProvider.value(value: Tip()),
        ChangeNotifierProvider.value(value: Post()),
        ChangeNotifierProvider.value(value: Comment()),
        ChangeNotifierProvider.value(value: Like()),
        ChangeNotifierProvider.value(value: notification()),
        ChangeNotifierProvider.value(value: FeelingCategorie()),
        ChangeNotifierProvider.value(value: Saving()),
      ],
      builder: (context, child) {
        return MaterialApp(
          navigatorKey: MyApp.navigatorKey,
          debugShowCheckedModeBanner: false,
          title: 'Support',
          theme: ThemeData(
            primarySwatch: CommentStyle.myThemeColor,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          initialRoute: '/',
          routes: routes,
        );
      },
    );
  }

  @override
  void initState() {

    LocalNotificationService.initialize(context);


  }
}
