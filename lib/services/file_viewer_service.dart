import 'dart:io';
import 'package:flutter/material.dart';
import 'package:support/services/audio.dart';
import 'package:support/services/video_player.dart';
class FileViewer extends StatefulWidget {
  String extension;
  File? file ;


  FileViewer(this.extension,{this.file});

  @override
  _FileViewerState createState() => _FileViewerState();
}

class _FileViewerState extends State<FileViewer> {
  @override
  Widget build(BuildContext context) {
    if (widget.extension == 'mp4') {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Container(
              height: 200,
              child: VideoPlayerScreen(file: widget.file),
            ),
          ],
        ),
      );
    } else if (widget.extension == 'm4a') {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            AudioPlayerUrl(localUrl: widget.file!.path),
          ],
        ),
      );
    } else
      return Container();
  }
}
