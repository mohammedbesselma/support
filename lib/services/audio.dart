
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:support/services/api_http.dart';
import 'package:support/utilities/comment_style.dart';

class AudioPlayerUrl extends StatefulWidget {
  late String? url ;
  late String? localUrl ;

  AudioPlayerUrl({this.url, this.localUrl});

  @override
  _AudioPlayerUrlState createState() => _AudioPlayerUrlState();
}

class _AudioPlayerUrlState extends State<AudioPlayerUrl> {
  /// For clarity, I added the terms compulsory and optional to certain sections
  /// to maintain clarity as to what is really needed for a functioning audio player
  /// and what is added for further interaction.
  ///
  /// 'Compulsory': A functioning audio player with:
  ///             - Play/Pause button
  ///
  /// 'Optional': A functioning audio player with:
  ///             - Play/Pause button
  ///             - time stamps for progress and duration
  ///             - slider to jump within the audio file
  ///
  /// Compulsory
  AudioPlayer audioPlayer = AudioPlayer();
  PlayerState audioPlayerState = PlayerState.PAUSED;


  /// Optional
  int timeProgress = 0;
  int audioDuration = 0;


  /// Optional
  Widget slider() {
    return Container(
      height: 30,

      width: MediaQuery.of(context).size.width-90,

      child: SliderTheme(
      data: SliderThemeData(
          thumbColor: Colors.white,
          inactiveTrackColor: Colors.grey,
          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 5)
      ),

      child: Slider.adaptive(
          value: timeProgress.toDouble(),

          max: audioDuration.toDouble(),
          onChanged: (value) {
            seekToSec(value.toInt());
          }
          ),
      )
    );
  }

  @override
  void initState() {

    super.initState();
    /// Compulsory
    audioPlayer.onPlayerStateChanged.listen((PlayerState state) {
      setState(() {
        audioPlayerState = state;
      });
    });

    /// Optional
    if (widget.localUrl == null){
      audioPlayer.setUrl(
          Network.host+'storage/'+widget.url!); // Triggers the onDurationChanged listener and sets the max duration string
      audioPlayer.onDurationChanged.listen((Duration duration) {
        setState(() {
          audioDuration = duration.inSeconds;
        });
      });
    }else {
      audioPlayer.setUrl(widget.localUrl!,isLocal: true);
      audioPlayer.onDurationChanged.listen((Duration duration) {
        setState(() {
          audioDuration = duration.inSeconds;
        });
      });
    }

    audioPlayer.onAudioPositionChanged.listen((Duration position) async {
      setState(() {
        timeProgress = position.inSeconds;

      });
    });
  }

  /// Compulsory
  @override
  void dispose() {
    audioPlayer.dispose();
    super.dispose();
  }

  /// Compulsory
  playMusic() async {
    // Add the parameter "isLocal: true" if you want to access a local file
    if(widget.localUrl == null){
      await audioPlayer.play(Network.host+'storage/'+widget.url!);
    }else  await audioPlayer.play(widget.localUrl!,isLocal: true);

  }

  /// Compulsory
  pauseMusic() async {
    await audioPlayer.pause();
  }

  /// Optional
  void seekToSec(int sec) {
    Duration newPos = Duration(seconds: sec);
    audioPlayer
        .seek(newPos); // Jumps to the given position within the audio file
  }

  /// Optional
  String getTimeString(int seconds) {
    String minuteString =
        '${(seconds / 60).floor() < 10 ? 0 : ''}${(seconds / 60).floor()}';
    String secondString = '${seconds % 60 < 10 ? 0 : ''}${seconds % 60}';
    return '$minuteString:$secondString'; // Returns a string with the format mm:ss
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
          height: 40,
          decoration: BoxDecoration(
            color: CommentStyle.homeBackgroundColor,
            borderRadius: BorderRadius.circular(24),
          ),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,

              children: [
                /// Compulsory
                Container(
                  width: 20,
                  child: IconButton(

                      onPressed: () {
                        audioPlayerState == PlayerState.PLAYING
                            ? pauseMusic()
                            : playMusic();
                      },
                      icon: Icon(audioPlayerState == PlayerState.PLAYING
                          ? Icons.pause_rounded
                          : Icons.play_arrow_rounded )),
                ),

                /// Optional
                Container( child: slider()),
                Text(getTimeString(audioDuration-timeProgress))
              ],
            ),
          )),
    );
  }

}