import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:support/model/post.dart';
import 'package:support/model/user.dart';
import 'package:support/page/post/create_post_page.dart';
import 'package:support/services/toast_service.dart';
import 'package:support/utilities/comment_style.dart';
class PostMenu extends StatefulWidget {
  double posy;
  double posx;
  Post post ;
  bool? deletedFromDetail ;
  PostMenu(this.post,this.posy, this.posx,{this.deletedFromDetail});



  @override
  _PostMenuState createState() => _PostMenuState();
}

class _PostMenuState extends State<PostMenu> {

  bool isPoster = false;
  Future<void> deletePost()async {

    Map<String, String> data = {
      'id' : widget.post!.id!,
    };
    Loader.show(context,
        progressIndicator: SpinKitFadingCircle(
          color: Colors.black,
          size: 80.0,
        ));
    bool response = await Provider.of<Post>(context, listen: false)
        .deletePost(data);

    if (response) {
      if (widget.deletedFromDetail!=null){
        Navigator.of(context).pop();
      }
      Navigator.of(context).pop();
      Loader.hide();
      ToastService.showSuccessToast('Post has been deleted');
    } else {
      Loader.hide();
    }

  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User>(
      future: Provider.of<User>(context,listen: false).getUserData(),
      builder:  (BuildContext context,AsyncSnapshot<User> snapshot){

        if(snapshot.hasData){
          if(snapshot.data!.id==widget.post.userId){
            isPoster = true;
          }
          return Stack(
            children: [
              Positioned(
                top: widget.posy-10,
                left: widget.posx-160,
                child: Container(
                  width: 160,

                  decoration: BoxDecoration(
                    color:  Color(0xFFEFFFF3),
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10),bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),

                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,

                      children: <Widget>[

                        isPoster ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: (){
                              deletePost();
                            },
                            child: Row(
                              children: [
                                Icon(Icons.delete_outline,size: 15,),
                                SizedBox(width: 8,),
                                Text('Delete this post',style: CommentStyle.smallTextStyle(),),
                              ],
                            ),
                          ),
                        ) : Container(),
                        isPoster? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: (){
                              Navigator.of(context).pop();
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => CreatePostPage(widget.post.byParctitioner!,post: widget.post,)),
                              );
                            },
                            child: Row(
                              children: [
                                Icon(Icons.edit,size: 15,),
                                SizedBox(width: 8,),
                                Text('Edit post',style: CommentStyle.smallTextStyle(),),
                              ],
                            ),
                          ),
                        ): Container(),
                        !isPoster ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Icon(Icons.check,size: 15,),
                              SizedBox(width: 8,),
                              Text('Show more post \nlike this',style: CommentStyle.smallTextStyle(),),
                            ],
                          ),
                        ): Container(),
                        !isPoster ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Icon(Icons.info_outline,size: 15,),
                              SizedBox(width: 8,),
                              Text('Report',style: CommentStyle.smallTextStyle(),),
                            ],
                          ),
                        ): Container(),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Icon(Icons.share,size: 15,),
                              SizedBox(width: 8,),
                              Text('Recommend',style: CommentStyle.smallTextStyle(),),
                            ],
                          ),
                        ),
                        isPoster? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Icon(Icons.remove_circle_outline,size: 15,),
                              SizedBox(width: 8,),
                              Text('Hide post',style: CommentStyle.smallTextStyle(),),
                            ],
                          ),
                        ) : Container(),
                        !isPoster ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Icon(Icons.info_outline,size: 15,),
                              SizedBox(width: 8,),
                              Text('Block this user',style: CommentStyle.smallTextStyle(),),
                            ],
                          ),
                        ): Container(),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
        }
        else return Container();
    },) ;
  }
}
