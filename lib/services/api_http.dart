import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Network {
  late http.Response response;

  late String token;

  static String version = 'api/';
  // static String host = 'http://34.69.4.138/';
  static String host = 'http://192.168.8.101/';



   static _setHeaders() =>
      {'Content-type': 'application/json', 'Accept': 'application/json'};


  _setHeadersWithToken() {
    return SharedPreferences.getInstance().then((storage) {
      this.token = storage.getString('access_token')!;
      return {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${this.token}'
      };
    });
  }
  Future<http.Response> postWithHeader(apiUrl, data) async {
    var fullUrl = host + version + apiUrl;
    final response = await http.post(Uri.parse(fullUrl),
        body: jsonEncode(data), headers: await _setHeadersWithToken());
    return response;
  }

  Future<http.Response> post(apiUrl, data) async {
    var fullUrl = host + version + apiUrl;
    final response = await http.post(Uri.parse(fullUrl),
        body: jsonEncode(data), headers: _setHeaders());
    return response;
  }



  Future<http.Response> get(apiUrl) async {
    var fullUrl = host + version + apiUrl;
    final response = await http.get(Uri.parse(fullUrl), headers: _setHeaders());
    return response;
  }
}
