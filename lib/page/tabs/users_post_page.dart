// ignore_for_file: unnecessary_new

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:support/model/catrgory.dart';
import 'package:support/model/post.dart';
import 'package:shimmer/shimmer.dart';
import 'package:support/page/tabs/widget/category_item_widget.dart';
import 'package:support/page/tabs/widget/post_item_widget.dart';
import 'package:support/utilities/comment_style.dart';

class UsersPosts extends StatefulWidget {
  const UsersPosts({Key? key}) : super(key: key);

  @override
  _UsersPostsState createState() => _UsersPostsState();
}

class _UsersPostsState extends State<UsersPosts> {
  List<Category> categoryList = Category.getCategory() ;
   late  int selectedCategory = 0 ;
   String selectedCategoryTitle ='all gategory';
   Future<void> _refresh() async {
     await Provider.of<Post>(context,listen: false).refresh();
   }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CommentStyle.homeBackgroundColor,
      child: Column(

        children: [
          Padding(
            padding: EdgeInsets.only(left: 15, top: 5, bottom: 5),
            child: InkWell(
              onTap: () {
                showModalBottomSheet(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20)),
                    ),
                    context: context,
                    builder: (context) {
                      return CategorySelector(categoryList);
                    });
              },
              child: Row(
                children: [
                  Text(
                    selectedCategoryTitle,
                    style: CommentStyle.smallTextStyle(),
                  ),
                  Icon(
                    Icons.keyboard_arrow_down,
                    size: 20,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: FutureBuilder<List<Post>>(
                future: Provider.of<Post>(context, listen: true).getPosts(idCtegory: selectedCategory),
                builder: (BuildContext context, AsyncSnapshot<List<Post>> snapshot) {

              if (snapshot.hasData) {
                if(snapshot.data!.length !=0){
                  return  RefreshIndicator(child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, i) {
                      if (snapshot.data![i].byParctitioner == '0'){

                        return PostItemWidget(snapshot.data![i]);

                      }else return Container();

                    },
                  ), onRefresh:_refresh );
                }else {
                  return Center(
                    child: Text('There is no post',style: CommentStyle.smallBoldFogyTxtStyle(),),
                  );
                }

              } else{
                return Center(
                  child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Shimmer.fromColors(
                        baseColor: Colors.green.shade50,
                        highlightColor: Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 30),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 15,
                                    width: 200,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    height: 10,
                                    width: 120,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Container(
                                    height: 100,
                                    width: MediaQuery.of(context).size.width - 20,
                                    color: Colors.grey,
                                  ),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 15,
                                    width: 200,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    height: 10,
                                    width: 120,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Container(
                                    height: 100,
                                    width: MediaQuery.of(context).size.width - 20,
                                    color: Colors.grey,
                                  ),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 15,
                                    width: 200,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    height: 10,
                                    width: 120,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Container(
                                    height: 100,
                                    width: MediaQuery.of(context).size.width - 20,
                                    color: Colors.grey,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }


                }),
          ),
        ],
      ),
    );
  }
  Widget CategorySelector(categoryList) {
    return StatefulBuilder(builder: (BuildContext context, StateSetter localSetState) {
      return Container(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Container(
                height: 6,
                width: 41,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3),
                    color: Color(0xFFCCCCCC)),
              ),
              Row(
                children: [
                  Text(
                    selectedCategoryTitle,
                    style: CommentStyle.smallBoldGreenTxtStyle(),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 1,
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xFFA9A9A9)),
              ),
              ListView.builder(
                  shrinkWrap: true,
                  primary: false,
                  itemCount: categoryList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: () {
                        setState(() {
                          categoryList
                              .forEach((element) => element.isSelected = false);
                          categoryList[index].isSelected = true;
                          selectedCategory = categoryList[index].id ;
                          selectedCategoryTitle = categoryList[index].categoryText ;
                          Navigator.of(context).pop();
                        });

                      },
                      child: CategoryItem(categoryList[index]),
                    );
                  })
            ],
          ),
        ),
      );
    });
  }
}
