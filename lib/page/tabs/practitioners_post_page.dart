import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:support/model/post.dart';
import 'package:support/model/practitionerpost.dart';
import 'package:support/page/tabs/widget/post_item_widget.dart';
import 'package:support/page/tabs/widget/practitioner_post_item.dart';
import 'package:support/services/file_extension_getter.dart';
import 'package:support/utilities/comment_style.dart';

class PractitionerPostPage extends StatefulWidget {
  const PractitionerPostPage({Key? key}) : super(key: key);

  @override
  _PractitionerPostPageState createState() => _PractitionerPostPageState();
}

class _PractitionerPostPageState extends State<PractitionerPostPage> {
  String mediaFormat = '.mp4';
  bool isSelectedVideo = true;
  bool isSelectedAudio = false;
  bool isSelectedPost = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CommentStyle.homeBackgroundColor,
      child: FutureBuilder<List<Post>>(
          future: Provider.of<Post>(context, listen: true).getPosts(),
          builder: (BuildContext context, AsyncSnapshot<List<Post>> snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 15, top: 5, bottom: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              mediaFormat = '.mp4';
                              isSelectedVideo = true;
                              isSelectedAudio = false;
                              isSelectedPost = false;
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: isSelectedVideo
                                    ? CommentStyle.myThemeColor
                                    : Colors.white),
                            height: 24,
                            width: 51,
                            child: Center(
                                child: Text(
                              'Video',
                              style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 10,
                                  fontWeight: FontWeight.w700,
                                  color: isSelectedVideo
                                      ? Colors.white
                                      : CommentStyle.fogytex),
                            )),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              mediaFormat = '.m4a';
                              isSelectedVideo = false;
                              isSelectedAudio = true;
                              isSelectedPost = false;
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: isSelectedAudio
                                    ? CommentStyle.myThemeColor
                                    : Colors.white),
                            height: 24,
                            width: 51,
                            child: Center(
                                child: Text(
                              'Audios',
                              style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 10,
                                  fontWeight: FontWeight.w700,
                                  color: isSelectedAudio
                                      ? Colors.white
                                      : CommentStyle.fogytex),
                            )),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              mediaFormat = 'null';
                              isSelectedVideo = false;
                              isSelectedAudio = false;
                              isSelectedPost = true;
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: isSelectedPost
                                    ? CommentStyle.myThemeColor
                                    : Colors.white),
                            height: 24,
                            width: 51,
                            child: Center(
                                child: Text(
                              'Posts',
                              style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 10,
                                  fontWeight: FontWeight.w700,
                                  color: isSelectedPost
                                      ? Colors.white
                                      : CommentStyle.fogytex),
                            )),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: snapshot.data!.length,
                      itemBuilder: (context, i) {


                        if (Extension().getExtension(snapshot.data![i].mediaLink) ==
                            mediaFormat && snapshot.data![i].byParctitioner == '1') {
                          return PractitionerPostItemWidget(
                              mediaForf: mediaFormat,
                              post: snapshot.data![i]);
                        } else
                          return Container(

                          );
                      },
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Icon(
                Icons.error_outline,
                color: Colors.red,
                size: 60,
              );
            } else {
              return Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Shimmer.fromColors(
                    baseColor: Colors.green.shade50,
                    highlightColor: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 30),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 15,
                                width: 200,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                height: 10,
                                width: 120,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Container(
                                height: 100,
                                width: MediaQuery.of(context).size.width - 20,
                                color: Colors.grey,
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 15,
                                width: 200,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                height: 10,
                                width: 120,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Container(
                                height: 100,
                                width: MediaQuery.of(context).size.width - 20,
                                color: Colors.grey,
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 15,
                                width: 200,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                height: 10,
                                width: 120,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Container(
                                height: 100,
                                width: MediaQuery.of(context).size.width - 20,
                                color: Colors.grey,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }
          }),
    );
  }
}
