import 'package:flutter/material.dart';
import 'package:support/model/catrgory.dart';
import 'package:support/utilities/comment_style.dart';

Widget CategoryItem(Category category) {
  return Padding(
    padding: EdgeInsets.all(10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          category.categoryText,
          style: category.isSelected
              ? CommentStyle.smallBoldBlackTxtStyle()
              : CommentStyle.smallFogyTxtStyle(),
        ),
        category.isSelected
            ? Icon(
                Icons.check,
                color: CommentStyle.myThemeColor,
              )
            : Container()
      ],
    ),
  );
}
