import 'package:flutter/material.dart';
import 'package:support/page/tabs/widget/category_item_widget.dart';
import 'package:support/utilities/comment_style.dart';

Widget CategorySelector(categoryList) {
  return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
    return Container(
      height: 300,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Container(
              height: 6,
              width: 41,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  color: Color(0xFFCCCCCC)),
            ),
            Row(
              children: [
                Text(
                  'Categories',
                  style: CommentStyle.smallBoldGreenTxtStyle(),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 1,
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xFFA9A9A9)),
            ),
            ListView.builder(
                shrinkWrap: true,
                itemCount: categoryList.length,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () {
                      setState(() {
                        categoryList
                            .forEach((element) => element.isSelected = false);
                        categoryList[index].isSelected = true;
                      });
                    },
                    child: CategoryItem(categoryList[index]),
                  );
                })
          ],
        ),
      ),
    );
  });
}
