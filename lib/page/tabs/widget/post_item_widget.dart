import 'package:flutter/material.dart';
import 'package:support/model/post.dart';
import 'package:support/model/user.dart';
import 'package:support/page/post/post_details_page.dart';
import 'package:support/services/show_post_menu.dart';
import 'package:support/utilities/comment_style.dart';
import 'package:support/widgets/post/comment_widget.dart';
import 'package:support/widgets/post/like_widget.dart';
import 'package:support/widgets/post/saving_widget.dart';

class PostItemWidget extends StatelessWidget {
  Post post;

  PostItemWidget(this.post);

  double posx = 100.0;
  double posy = 100.0;

  void onTapDown(TapDownDetails details, StateSetter localsetter) {
    var x = details.globalPosition.dx;
    var y = details.globalPosition.dy;
    // or user the local position method to get the offset
    print(details.localPosition);
    print("tap down " + x.toString() + ", " + y.toString());
    localsetter(() {
      posy = y;
      posx = x;
    });
  }


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(PostDetaisPage.routeName, arguments: post);
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Container(
          decoration: BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(
                color: Colors.grey.shade100,
                blurRadius: 5.0,
                offset: Offset(0.0, 0.75)
            )
          ]),
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      top: 20, left: 2, right: 2, bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            post.username!,
                            style: CommentStyle.smallBoldGreenTxtStyle(),
                          ),
                          Text(
                            CommentStyle.getTextFromDate(post.createdAt!),
                            style: CommentStyle.extraSmallFogyTxtStyle(),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          SavingWidget(post.id.toString()),
                          StatefulBuilder(builder:
                              (BuildContext context, StateSetter local) {
                            return Container(
                              width: 30,
                              height: 30,
                              child: InkWell(
                                onTapDown:
                                    (TapDownDetails details) =>
                                    onTapDown(details, local),
                                onTap: () {
                                  showDialog<void>(
                                    barrierColor: Colors.transparent,
                                    barrierDismissible: true,
                                    context: context,
                                    builder: (BuildContext context) {
                                      return PostMenu(post!,posy,posx);
                                    },
                                  );
                                },
                                child:
                                Image.asset('assets/icons/dots_icon.png'),
                              ),
                            );
                          }),
                        ],
                      )
                    ],
                  ),
                ),
                Text(
                  post.content!,
                  style: CommentStyle.smallTextStyle(),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        LikeWidget(post),
                        SizedBox(
                          width: 20,
                        ),
                        CommentWidget(post.comments!.length),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(Icons.share,size: 20,color: CommentStyle.myThemeColor,),
                        SizedBox(width: 10,),
                        Text('Recommend',style: CommentStyle.smallTextStyle(),)
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

