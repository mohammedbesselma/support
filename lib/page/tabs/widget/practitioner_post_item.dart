import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:support/model/post.dart';
import 'package:support/model/practitionerpost.dart';
import 'package:support/page/post/post_details_page.dart';
import 'package:support/services/audio.dart';
import 'package:support/services/audio_player.dart';
import 'package:support/services/show_post_menu.dart';
import 'package:support/services/video_player.dart';
import 'package:support/utilities/comment_style.dart';
import 'package:support/widgets/post/like_widget.dart';
import 'package:support/widgets/post/saving_widget.dart';

class PractitionerPostItemWidget extends StatefulWidget {
  String? mediaForf;
  Post? post;

  PractitionerPostItemWidget({this.mediaForf, this.post});

  @override
  _PractitionerPostItemWidgetState createState() =>
      _PractitionerPostItemWidgetState();
}

class _PractitionerPostItemWidgetState
    extends State<PractitionerPostItemWidget> {
  double posx = 100.0;
  double posy = 100.0;

  void onTapDown(TapDownDetails details, StateSetter localsetter) {
    var x = details.globalPosition.dx;
    var y = details.globalPosition.dy;
    // or user the local position method to get the offset
    print(details.localPosition);
    print("tap down " + x.toString() + ", " + y.toString());
    localsetter(() {
      posy = y;
      posx = x;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
              color: Colors.grey.shade100,
              blurRadius: 5.0,
              offset: Offset(0.0, 0.75))
        ]),
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(PostDetaisPage.routeName,
                    arguments: widget.post);
              },
              child: Padding(
                padding:
                    EdgeInsets.only(top: 20, bottom: 5, right: 10, left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.post!.postTitle!,
                              style: CommentStyle.smallBoldGreenTxtStyle(),
                            ),
                            Text(
                              CommentStyle.getTextFromDate(
                                  widget.post!.createdAt!),
                              style: CommentStyle.extraSmallFogyTxtStyle(),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            SavingWidget(widget.post!.id.toString()),
                            StatefulBuilder(builder:
                                (BuildContext context, StateSetter local) {
                              return Container(
                                width: 30,
                                height: 30,
                                child: InkWell(
                                    onTapDown:
                                        (TapDownDetails details) =>
                                        onTapDown(details, local),
                                  onTap: () {
                                    showDialog<void>(
                                      barrierColor: Colors.transparent,
                                      barrierDismissible: true,
                                      context: context,
                                      builder: (BuildContext context) {
                                        return PostMenu(widget.post!,posy,posx);
                                      },
                                    );
                                  },
                                  child:
                                      Image.asset('assets/icons/dots_icon.png'),
                                ),
                              );
                            }),
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      widget.post!.content!,
                      style: CommentStyle.smallTextStyle(),
                    ),
                  ],
                ),
              ),
            ),
            widget.mediaForf! == '.mp4'
                ? new VideoPlayerScreen(
                    mediaLink: widget.post!.mediaLink!
                        .substring(7, widget.post!.mediaLink!.length),
                  )
                : widget.mediaForf! == '.m4a'
                    ? AudioPlayerUrl(
                        url: widget.post!.mediaLink!
                            .substring(7, widget.post!.mediaLink!.length))
                    : Container(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Row(
                        children: [
                          LikeWidget(widget.post!),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.share,
                        size: 20,
                        color: CommentStyle.myThemeColor,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Recommend',
                        style: CommentStyle.smallTextStyle(),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

