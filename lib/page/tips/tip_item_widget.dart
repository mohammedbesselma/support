import 'package:flutter/material.dart';
import 'package:support/model/tip.dart';
import 'package:support/utilities/comment_style.dart';

class TipItemWidget extends StatefulWidget {
  Tip tip ;
  TipItemWidget(this.tip);

  @override
  _TipItemWidgetState createState() => _TipItemWidgetState();
}

class _TipItemWidgetState extends State<TipItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2),
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.tip.title!,
                style: CommentStyle.smallBoldGreenTxtStyle(),
              ),
              SizedBox(height: 8,),
              Text(
                widget.tip.content!,
                style: CommentStyle.smallTextStyle(),
              ),
              SizedBox(height: 12,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.share,size: 20,color: CommentStyle.myThemeColor,),
                  SizedBox(width: 10,),
                  Text('Recommend',style: CommentStyle.smallTextStyle(),)
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
