import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:support/model/tip.dart';
import 'package:support/page/exercice/exercice_selection_page.dart';
import 'package:support/page/tips/tip_item_widget.dart';
import 'package:support/utilities/comment_style.dart';
class TipsPage extends StatefulWidget {

  const TipsPage({Key? key}) : super(key: key);

  @override
  _TipsPageState createState() => _TipsPageState();
}

class _TipsPageState extends State<TipsPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: CommentStyle.homeBackgroundColor,
        floatingActionButton: Container(
          height: 80,
          width: 80,
          child: FloatingActionButton(
            onPressed: () {
              Navigator.of(context).pushNamed(ExerciceSelectionPage.routeName);
            },
            child: Column (
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

              Image.asset('assets/icons/exerciceicon.png'),
              SizedBox(height: 4,),
              Text('Breathing\nTraning',style: CommentStyle.smallWhiteTxtStyle(),textAlign: TextAlign.center,)

            ],),
            backgroundColor: CommentStyle.myThemeColor,
          ),
        ),
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Color(0xFF7ECF94),
          title: Center(child: Text('Here are the daily tips for you',style: CommentStyle.smallBoldBlackTxtStyle(),)),
        ),
        body: FutureBuilder<List<Tip>>(
          future: Provider.of<Tip>(context,listen: false).getTips(),
          builder: (BuildContext context , AsyncSnapshot<List<Tip>> snapshot){
            switch (snapshot.connectionState){

              case ConnectionState.waiting :
                return Center(child: Container(
                  height: 40,
                  width: 40,
                  child: CircularProgressIndicator(),
                ));
              default :
                if (snapshot.hasData){

                  if(snapshot.data!.length != 0){

                    return Container(
                      child: ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemCount: snapshot.data!.length,
                          itemBuilder: (BuildContext context, int index) {
                            return TipItemWidget(snapshot.data![index]);
                          }),
                    );
                  }else {
                    return Center(
                      child: Text('No tips found',style: CommentStyle.smallBoldFogyTxtStyle(),),
                    );
                  }


                }else return Container();
            }

          },
        ),
      ),
    );
  }
}
