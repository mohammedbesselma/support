import 'package:flutter/material.dart';
import 'package:support/model/notification.dart';
import 'package:support/utilities/comment_style.dart';
class NotificationItemWidget extends StatefulWidget {
  notification not ;


  NotificationItemWidget(this.not);

  @override
   _NotificationItemWidgetState createState() => _NotificationItemWidgetState();
 }

 class _NotificationItemWidgetState extends State<NotificationItemWidget> {
   @override
   Widget build(BuildContext context) {
     return Container(

       child: Padding(
         padding: EdgeInsets.all(8.0),
         child: Row(

           mainAxisAlignment: MainAxisAlignment.spaceBetween,

           children: [
             Stack(
               children: [
                 Container(
                   width: 60.0,
                   height: 60.0,
                   decoration: BoxDecoration(

                     borderRadius: BorderRadius.all(Radius.circular(60.0)),
                   ),
                   child: Image.asset('assets/images/largeprofile.png'),
                 ),
                 Positioned(
                   right: 0,
                   bottom: 0,
                   child: Container(
                     decoration: new BoxDecoration(
                       borderRadius: new BorderRadius.circular(25),
                       border: Border.all(color: Colors.grey),
                       color: Colors.grey[200],
                     ),
                     width: 20,
                     height: 20,
                     child: Icon(
                           Icons.notifications,
                           size: 15,
                         ),
                   ),
                 ),
               ],
             ),

             Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: [
                Container( width: MediaQuery.of(context).size.width-120, child: CommentStyle.getNotText(widget.not.username!, widget.not.content!)),
                Text(widget.not.createdAt!,style: CommentStyle.smallBoldFogyTxtStyle(),)
               ],
             ),
             InkWell(

               onTap: (){

               },
               child: Container(
                   height: 30,
                   width: 20,
                   child: Image.asset('assets/icons/dots_icon.png',color: Color(0xFF545454),)),
             ),
           ],

         ),
       ),
     );
   }
 }
