import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:support/model/notification.dart';
import 'package:support/page/notification/notification_item_widget.dart';
import 'package:support/utilities/comment_style.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(child: Padding(
              padding: const EdgeInsets.all(20),
              child: Text('Notifications',style: CommentStyle.largBoldGreenTxtStyle(),),
            )),
            Expanded(
              child: FutureBuilder<List<notification>>(
                future: Provider.of<notification>(context,listen: true).getNotification(),
                builder: (BuildContext context,AsyncSnapshot<List<notification>> snapshot){
                  if(snapshot.hasData){
                    if(snapshot.data!.length!=0){;

                      return ListView.builder(
                          shrinkWrap: true,
                          itemCount:snapshot.data!.length,
                          itemBuilder: (BuildContext context, int index) {
                            return NotificationItemWidget(snapshot.data![index]);
                          });

                    }else {
                      return Center(child: Text('No notification found',style: CommentStyle.smallBoldFogyTxtStyle(),),);
                    }

                  }else  return Center(
                    child: Container(
                      height: 40,
                      width: 40,
                      child: CircularProgressIndicator(),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
