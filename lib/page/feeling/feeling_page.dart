import 'package:flutter/material.dart';
import 'package:support/page/feeling/feeling_item.dart';
import 'package:support/utilities/comment_style.dart';
import 'package:support/utilities/validate.dart';

class FeelingPage extends StatefulWidget {
  static const routeName = 'feeling_page';
  const FeelingPage({Key? key}) : super(key: key);

  @override
  _FeelingPageState createState() => _FeelingPageState();
}

class _FeelingPageState extends State<FeelingPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late String content;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child : Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);
                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: Container(
                height: MediaQuery.of(context).size.height-100,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 20),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Let Us know how you are feeling today ? ',style: CommentStyle.smallBoldBlackTxtStyle(),),
                        GridView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 4,
                            childAspectRatio: 2.2,
                            mainAxisSpacing: 10.0,
                            crossAxisSpacing: 10.0,
                          ),
                          itemCount: 15,
                          itemBuilder: (context, index) {

                            return FeelingItem(index);
                          },
                        ),
                        TextFormField(
                          decoration: CommentStyle.feelingTextField(),
                          keyboardType: TextInputType.multiline,
                          minLines:
                          1, //Normal textInputField will be displayed
                          maxLines: null,
                          validator: (value) {
                            content = value!.trim();

                            return Validate.requiredField(
                                value, 'content is required.');
                          }, // when user presses enter
                        ),
                        Container(
                          height: 35,
                          width: 90,
                          decoration: BoxDecoration(
                              color: CommentStyle.myThemeColor,
                              borderRadius: BorderRadius.circular(20)
                          ),
                          child: FlatButton(
                            onPressed: (){
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Send',style: CommentStyle.smallWhiteTxtStyle(),),
                                  Icon(Icons.send,size: 14,color: Colors.white,)
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              ),
          ),
          ),


    );
  }
}
