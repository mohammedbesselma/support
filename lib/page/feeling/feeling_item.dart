import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:support/model/feelingcategorie.dart';
import 'package:support/utilities/comment_style.dart';
class FeelingItem extends StatelessWidget {
  int id;

  FeelingItem(this.id);

  @override
  Widget build(BuildContext context) {
    return Container(
      child:  Consumer<FeelingCategorie>(
        builder: (context, cat, child) {
          return InkWell(
              onTap: (){
                Provider.of<FeelingCategorie>(context,listen: false).selecteCategorie(id+1);
                print(id.toString());

              },
              child: Container(

                  decoration: BoxDecoration(
                    color: FeelingCategorie.feelingcatigories[id].isSelected != false ? Colors.black : Colors.white  ,
                    border: Border.all(color: FeelingCategorie.feelingcatigories[id].isSelected != false ? Colors.black :  Colors.grey,width: 1),
                    borderRadius: BorderRadius.circular(20)
                  ),

                  child: Center(child: Container(
                      width: 60,
                      child: Center(child: Text(FeelingCategorie.feelingcatigories[id].categoryText.toString(),style: FeelingCategorie.feelingcatigories[id].isSelected != false ? CommentStyle.smallWhiteTxtStyle() :  CommentStyle.extraSmallFogyTxtStyle(),))))
              ));
        },
      ),
    );
  }
}
