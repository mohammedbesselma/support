import 'package:flutter/material.dart';
import 'package:support/model/catrgory.dart';
import 'package:support/page/exercice/exercice_page.dart';
import 'package:support/utilities/comment_style.dart';
class TipItemWidget extends StatefulWidget {
  Category category;

  TipItemWidget(this.category);



  @override
  _TipItemWidgetState createState() => _TipItemWidgetState();
}

class _TipItemWidgetState extends State<TipItemWidget> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        showDialog(context: context,
            builder: (BuildContext context){
              return Center(

                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 50),
                  child: Stack(
                    children: [

                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20),

                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text("Are you ready? \nRelax.",style: CommentStyle.mediumBoldGreenTxtStyle(),),
                                SizedBox(height: 15,),
                                Container(

                                  height: 45,
                                  width: 90,

                                  decoration: BoxDecoration(
                                        color: CommentStyle.myThemeColor,
                                        borderRadius: BorderRadius.circular(20)
                                    ),
                                  child: FlatButton(
                                    onPressed: (){
                                      Navigator.of(context).pop();
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => ExercicePage(widget.category)),
                                      );
                                    },
                                    child: Text('Start',style: CommentStyle.smallBoldWhiteTxtStyle(),),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                          right: 0,
                          top: 0,
                          child: GestureDetector(
                              onTap: (){
                                Navigator.of(context).pop();
                              },
                              child: Container(
                                height: 25,
                                  width: 25,
                                  decoration: BoxDecoration(
                                      color: Colors.black,
                                      borderRadius: BorderRadius.all(Radius.circular(20))
                                  ),
                                  child: Icon(Icons.close,size: 20,color: Colors.white,)))),
                    ],
                  ),
                ),
              );
            }
        );
      },
      child: Container(
        child: Card(
          elevation: 3,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: [Colors.white, Colors.white, widget.category.color])),
            width: 100,
            height: 100,
            child: Align(
              alignment:Alignment(-0.4, 0.6),
              child: Text(widget.category.categoryText,style: CommentStyle.MediumBoldBlackTxtStyle(),),
            ),
          ),
        ),
      ),
    );
  }
}
