import 'package:flutter/material.dart';
import 'package:support/model/tip.dart';
import 'package:support/page/exercice/exercice_page.dart';
import 'package:support/page/exercice/tip_item_widget.dart';
import 'package:support/utilities/comment_style.dart';
import 'package:support/model/catrgory.dart';
class ExerciceSelectionPage extends StatefulWidget {
  static const routeName = 'exercice_selection_page';
  const ExerciceSelectionPage({Key? key}) : super(key: key);

  @override
  _ExerciceSelectionPageState createState() => _ExerciceSelectionPageState();
}

class _ExerciceSelectionPageState extends State<ExerciceSelectionPage> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor:Colors.white,
          title: Text('What do you want to reduce?',style: CommentStyle.MediumBoldBlackTxtStyle(),),
          leading: Container(
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: CommentStyle.myThemeColor,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )),
        ),
        body: Padding(
          padding: const EdgeInsets.all(20),
          child: GridView.builder(
              shrinkWrap: true,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20
              ),
              itemCount: catigories.length,
              itemBuilder: (BuildContext context, int index) {
                return TipItemWidget(catigories[index]);
              }
          ),
        ),
      ),
    );
  }



}
