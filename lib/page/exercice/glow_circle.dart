import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:support/utilities/comment_style.dart';

//apply this class on home attribute of Material App at main.dart
class GlowCircle extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _GlowCircle();
  }
}

class _GlowCircle extends State<GlowCircle>with SingleTickerProviderStateMixin {
  //use "with SingleThickerProviderStateMixin" at last of class declaration
  //where you have to pass "vsync" argument, add this

  late Animation<double> animation; //animation variable for circle 1
  late AnimationController animationcontroller; //animation controller variable circle 1

  @override
  void initState() {
    animationcontroller = AnimationController(vsync: this, duration: Duration(seconds: 1));
    //here we have to vash vsync argument, there for we use "with SingleTickerProviderStateMixin" above
    //vsync is the ThickerProvider, and set duration of animation


    //repeat the animation controller

    animation = Tween <double>(begin: 150,end: 200).animate(animationcontroller);
    //set the begin value and end value, we will use this value for height and width of circle

    animation.addListener(() {
      setState(() { });
      //set animation listiner and set state to update UI on every animation value change
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    animationcontroller.dispose(); //destory anmiation to free memory on last
  }

  @override
  Widget build(BuildContext context) {
    animationcontroller.repeat();
    return Stack(
      children: [

        Center(
          child: Container(
                decoration: BoxDecoration(
                    shape:BoxShape.circle, //making box to circle
                    color:Color(0xFFB4E5E7) //background of container
                ),
                height:animation.value, //value from animation controller
                width: animation.value,
                ),
        ),
        //value from animation controller

        Center(
          child: Container(
            decoration: BoxDecoration(
                shape:BoxShape.circle, //making box to circle
                color:Color(0xFF83C2C5) //background of container
            ),
            height:150, //value from animation controller
            width: 150,
            child: Center(child: Text('5',style: CommentStyle.largeWhiteTxtStyle(),),
            ),//value from animation controller
          ),
        ),
      ],
    );
  }
}