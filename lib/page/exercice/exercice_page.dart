import 'dart:async';

import 'package:flutter/material.dart';
import 'package:support/model/catrgory.dart';
import 'package:support/page/exercice/glow_circle.dart';
import 'package:support/utilities/comment_style.dart';

class ExercicePage extends StatefulWidget {
  late Category category;

  ExercicePage(this.category);

  @override
  _ExercicePageState createState() => _ExercicePageState();
}

class _ExercicePageState extends State<ExercicePage>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation; //animation variable for circle 1
  late AnimationController animationcontroller;
  late int minute = 0;
  late int second = 0;
  late Duration duration;
  late int sec;
  late int min;
  late int breathCounet = 0;
  String breath = '';

  //animation controller variable circle 1

  String _printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  late Timer _timer;
  void startTimer()async {
    const oneSec = const Duration(seconds: 1);

    animationcontroller.repeat();
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {

        if (sec%5==0){
          setState(() {
            breathCounet--;
          });
          if(sec%10==0){
            setState(() {
              breath = 'Breath in ';
            });
          }else {
            setState(() {
              breath = 'Breath out ';
            });
          }

        }
        if (sec >= 60) {
          setState(() {
            second = sec % 60;
            minute = sec ~/ 60 - 1;
            sec--;
          });
        } else if (sec >= 0) {
          setState(() {
            second = sec % 60;
            minute = sec ~/ 60;
            sec--;
          });
        } else {
          {
            setState(() {
              animationcontroller.reset();
              breath = 'Exercice termined';
              timer.cancel();
            });
          }
        }

      },
    );
  }

  @override
  void initState() {
    duration = widget.category.duration;
    sec = duration.inSeconds;
    breathCounet = sec  ~/ 5 +1;


    animationcontroller =
        AnimationController(vsync: this, duration: Duration(seconds: 5),);
    //here we have to vash vsync argument, there for we use "with SingleTickerProviderStateMixin" above
    //vsync is the ThickerProvider, and set duration of animation

    //repeat the animation controller

    animation =
        Tween<double>(begin: 150, end: 170).animate(animationcontroller);
    //set the begin value and end value, we will use this value for height and width of circle


    animation.addListener(() {
      setState(() {});
    });


    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    animationcontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: CommentStyle.homeBackgroundColor,
        leading: Container(
            child: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: CommentStyle.myThemeColor,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            minute.toString().padLeft(2, "0") +
                ':' +
                second.toString().padLeft(2, "0"),
            style: CommentStyle.MediumBoldBlackTxtStyle(),
          ),
          Container(
            height: 200,
            child: Center(
              child: Stack(
                children: [
                  Center(
                    child: Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, //making box to circle
                          color: Color(0xFFB4E5E7) //background of container
                          ),
                      height: animation.value, //value from animation controller
                      width: animation.value,
                    ),
                  ),
                  //value from animation controller

                  Center(
                    child: GestureDetector(
                      onTap: () {
                        if (_timer.isActive) {
                          setState(() {
                            animationcontroller.stop();
                            _timer.cancel();
                          });

                        } else {
                          animationcontroller.repeat();
                          startTimer();
                        }
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, //making box to circle
                            color: Color(0xFF83C2C5) //background of container
                            ),
                        height: 120, //value from animation controller
                        width: 120,
                        child: _timer.isActive? Center(
                          child: Text(
                            breathCounet.toString(),
                            style: CommentStyle.largeWhiteTxtStyle(),
                          ),
                        ) : Icon(Icons.pause,color: Colors.white,size: 30,), //value from animation controller
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Text(breath,style: CommentStyle.largBoldGreenTxtStyle(),)
        ],
      ),
    );
  }
}
