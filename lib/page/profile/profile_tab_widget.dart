import 'package:flutter/material.dart';
import 'package:support/model/user.dart';
import 'package:support/page/profile/myposts_list_widget.dart';
import 'package:support/utilities/comment_style.dart';
class ProfileTabWidget extends StatefulWidget {
  User user ;


  ProfileTabWidget(this.user);

  @override
  _ProfileTabWidgetState createState() => _ProfileTabWidgetState();
}

class _ProfileTabWidgetState extends State<ProfileTabWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: DefaultTabController(
        length: 2,
        child: Column(
          children: [
            Container(
              height: 30,
              width: 200,
              child: TabBar(
                isScrollable: false,
                indicatorSize: TabBarIndicatorSize.label,
                labelStyle: CommentStyle.tabItemTxtStyle(),
                labelColor: CommentStyle.myThemeColor,
                unselectedLabelColor: Color(0xFFA9A9A9),
                tabs: [
                  Stack(
                    alignment: AlignmentDirectional.bottomEnd,
                    children: [
                      Tab(
                        text: 'Posts',
                      ),
                    ],
                  ),
                  Stack(
                    alignment: AlignmentDirectional.bottomStart,
                    children: [
                      Tab(
                        text: 'About',
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height/2-80,
              child: TabBarView(
                children: [
                  MyPostListWidget(widget.user),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('full name :',style: CommentStyle.smallFogyTxtStyle()),
                          Text(widget.user.name!,style: CommentStyle.smallBoldGreenTxtStyle()),
                          SizedBox(height: 20,),
                          Text('email :',style: CommentStyle.smallFogyTxtStyle()),
                          Text(widget.user.email!,style: CommentStyle.smallBoldGreenTxtStyle(),),
                          SizedBox(height: 20,),
                          Text('phone :',style: CommentStyle.smallFogyTxtStyle()),
                          Text(widget.user.phone!,style: CommentStyle.smallBoldGreenTxtStyle(),),

                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
