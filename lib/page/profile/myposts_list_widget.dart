import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:support/model/post.dart';
import 'package:support/model/user.dart';
import 'package:support/page/profile/mypost_item_widget.dart';
import 'package:support/page/tabs/widget/practitioner_post_item.dart';
import 'package:support/services/file_extension_getter.dart';
class MyPostListWidget extends StatefulWidget {
  User user;


  MyPostListWidget(this.user);

  @override
  _MyPostListWidgetState createState() => _MyPostListWidgetState();
}

class _MyPostListWidgetState extends State<MyPostListWidget> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Post>>(
      future: Provider.of<Post>(context,listen: true).getPosts(idUser: widget.user.id),
      builder: (BuildContext context,AsyncSnapshot<List<Post>> snapshot){
        if(snapshot.hasData){
          return ListView.builder(
              shrinkWrap: true,
              itemCount:snapshot.data!.length,
              itemBuilder: (BuildContext context, int index) {
                print (snapshot.data![index].mediaLink);
                if (snapshot.data![index].byParctitioner == '1'  ){

                  return PractitionerPostItemWidget(post: snapshot.data![index],mediaForf: Extension().getExtension(snapshot.data![index].mediaLink));
                }else  return MyPostsItem(widget.user,snapshot.data![index]) ;
              });

        }
        else return Container();

      },
    );
  }
}
