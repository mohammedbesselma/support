import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:support/model/user.dart';
import 'package:support/services/toast_service.dart';
import 'package:support/utilities/comment_style.dart';
import 'package:support/utilities/validate.dart';

class UpdateProfilePage extends StatefulWidget {
  static const routeName = 'update_profile_page';
  const UpdateProfilePage({Key? key}) : super(key: key);

  @override
  _UpdateProfilePageState createState() => _UpdateProfilePageState();
}

class _UpdateProfilePageState extends State<UpdateProfilePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  double itemspace = 20;
  late String name;
  late String email;
  late String password;
  late String phone;
  bool isChecked = false;

  Future<void> updateProfile(id)async{
    final form = _formKey.currentState;

    if(form!.validate()){
      Map<String, String> data = {
        'id' : id,
        'name': name,
        'email': email,
        'phone': phone,

      };
      Loader.show(context,
          progressIndicator: SpinKitFadingCircle(
            color: Colors.black,
            size: 80.0,
          ));
      bool response = await Provider.of<User>(context, listen: false)
          .updateUser(data);

      if (response) {
        Navigator.of(context).pop();
        Loader.hide();
        ToastService.showSuccessToast('user data is saved successfully');
      } else {
        Loader.hide();
      }




    }

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          appBar: AppBar(
          elevation: 0,
          backgroundColor:Colors.white,
          title: Text('Update profile',style: CommentStyle.smallBoldBlackTxtStyle(),),
          leading: Container(
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: CommentStyle.myThemeColor,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )),
        ),
          body: FutureBuilder<User>(future: Provider.of<User>(context,listen: true).getUserData(),
          builder: (BuildContext context,AsyncSnapshot<User> snapshot){
         if (snapshot.hasData){
          return SingleChildScrollView(
            child: GestureDetector(
              child: Container(
                child: Form(
                  key: _formKey,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 100,horizontal: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Name : ',style: CommentStyle.smallBoldFogyTxtStyle(),),
                        TextFormField(
                          initialValue: snapshot.data!.name,
                          keyboardType: TextInputType.text,
                          decoration: CommentStyle.textFieldDecoration('Full name'),
                          validator: (value) {
                            name = value!.trim();
                            return Validate.requiredField(value, "name required");
                          },
                        ),
                        SizedBox(
                          height: itemspace,
                        ),
                        Text('Email : ',style: CommentStyle.smallBoldFogyTxtStyle(),),
                        TextFormField(
                          initialValue: snapshot.data!.email,
                          keyboardType: TextInputType.text,
                          decoration: CommentStyle.textFieldDecoration('Email'),
                          validator: (value) {
                            email = value!.trim();
                            return Validate.requiredField(value, "email required");
                          },
                        ),
                        SizedBox(
                          height: itemspace,
                        ),
                        Text('Phone :',style: CommentStyle.smallBoldFogyTxtStyle(),),
                        TextFormField(
                          initialValue: snapshot.data!.phone,
                          keyboardType: TextInputType.phone,
                          decoration: CommentStyle.textFieldDecoration('phone'),
                          validator: (value) {
                            phone = value!.trim();
                            return Validate.requiredField(value, "phone required");
                          },
                        ),
                        SizedBox(height: 20,),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            decoration: BoxDecoration(
                                color: CommentStyle.myThemeColor,
                                borderRadius: BorderRadius.circular(20)
                            ),
                            child: FlatButton(
                              onPressed: (){
                                updateProfile(snapshot.data!.id);

                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Update',style: CommentStyle.smallBoldWhiteTxtStyle(),),
                              ),
                            ),
                          ),
                        )

                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        }else return Container();

        },
      ),
    ));
  }
}
