import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:support/model/post.dart';
import 'package:support/model/user.dart';
import 'package:support/services/show_post_menu.dart';
import 'package:support/services/toast_service.dart';
import 'package:support/utilities/comment_style.dart';
import 'package:support/widgets/post/comment_widget.dart';
import 'package:support/widgets/post/like_widget.dart';
import 'package:support/widgets/post/saving_widget.dart';
class MyPostsItem extends StatefulWidget {
  Post post ;
  User user ;
  MyPostsItem(this.user,this.post);



  @override
  _MyPostsItemState createState() => _MyPostsItemState();
}

class _MyPostsItemState extends State<MyPostsItem> {
  double posx = 100.0;
  double posy = 100.0;
  Future<void> deletePost()async {

    Map<String, String> data = {
      'id' : widget.post!.id!,
    };
    Loader.show(context,
        progressIndicator: SpinKitFadingCircle(
          color: Colors.black,
          size: 80.0,
        ));
    bool response = await Provider.of<Post>(context, listen: false)
        .deletePost(data);

    if (response) {
      Navigator.of(context).pop();
      Loader.hide();
      ToastService.showSuccessToast('Post has been deleted');
    } else {
      Loader.hide();
    }

  }

  void onTapDown(TapDownDetails details,StateSetter localsetter) {

    var x = details.globalPosition.dx;
    var y = details.globalPosition.dy;
    // or user the local position method to get the offset
    print(details.localPosition);
    print("tap down " + x.toString() + ", " + y.toString());
    localsetter((){
      posy = y;
      posx = x;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
              color: Colors.grey.shade100,
              blurRadius: 5.0,
              offset: Offset(0.0, 0.75)
          )
        ]),
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    top: 20, left: 2, right: 2, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                           widget.user.userName!,
                          style: CommentStyle.smallBoldGreenTxtStyle(),
                        ),
                        Text(
                          CommentStyle.getTextFromDate(widget.post.createdAt!),
                          style: CommentStyle.extraSmallFogyTxtStyle(),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        SavingWidget(widget.post.id.toString()),
                        SizedBox(width: 20,),
                        StatefulBuilder(builder:(BuildContext context, StateSetter localsetState){
                          return  Container(
                            child: InkWell(

                              onTapDown: (TapDownDetails details) => onTapDown(details,localsetState),
                              onTap: (){
                                showDialog<void>(
                                  barrierColor: Colors.transparent,
                                  barrierDismissible: true,
                                  context: context,
                                  builder: (BuildContext context) {
                                    return PostMenu(widget.post,posy,posx);
                                  },
                                );
                              },
                              child: Container(
                                  height: 30,
                                  width: 20,
                                  child: Image.asset('assets/icons/dots_icon.png',color: Color(0xFF545454),)),
                            ),

                          );
                        })

                      ],
                    )
                  ],
                ),
              ),
              Text(
                widget.post.content!,
                style: CommentStyle.smallTextStyle(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      LikeWidget(widget.post),
                      SizedBox(
                        width: 20,
                      ),
                      CommentWidget(widget.post.comments!.length),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(Icons.share,size: 20,color: CommentStyle.myThemeColor,),
                      SizedBox(width: 10,),
                      Text('Recommend',style: CommentStyle.smallTextStyle(),)
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
