import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:support/model/user.dart';
import 'package:support/page/profile/update_profile_page.dart';
import 'package:support/providers/auth_provider.dart';
import 'package:support/utilities/comment_style.dart';
class HeaderWidget extends StatefulWidget {

  User user ;

  HeaderWidget(this.user);

  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(

      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [ Colors.amber.shade50,Colors.white, Colors.white])),

      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('assets/images/largeprofile.png'),
          SizedBox(height: 20,),
          Text(widget.user.name!,style: CommentStyle.smallBoldGreenTxtStyle(),),
          SizedBox(height: 20,),
          Container(
            decoration: BoxDecoration(
                color: CommentStyle.myThemeColor,
                borderRadius: BorderRadius.circular(20)
              ),
            child: FlatButton(
              onPressed: (){
                Navigator.of(context).pushNamed(UpdateProfilePage.routeName);
              },
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Edit profile',style: CommentStyle.smallBoldWhiteTxtStyle(),),
                ),
            ),
          ),
          SizedBox(height: 20,),
          Container(
            decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(20)
            ),
            child: FlatButton(
              onPressed: (){
                Provider.of<AuthProvider>(context, listen: false)
                    .logout();
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Logout',style: CommentStyle.smallBoldWhiteTxtStyle(),),
              ),
            ),
          )
        ],
      ),

    );
  }
}
