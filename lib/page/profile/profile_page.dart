import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:support/model/user.dart';
import 'package:support/page/profile/header_widget.dart';
import 'package:support/page/profile/profile_tab_widget.dart';
import 'package:support/page/tabs/practitioners_post_page.dart';
import 'package:support/page/tabs/users_post_page.dart';
import 'package:support/utilities/comment_style.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: FutureBuilder<User>(
              future: Provider.of<User>(context, listen: true).getUserData(),
              builder: (BuildContext cnx, AsyncSnapshot<User> snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    children: [
                      HeaderWidget(snapshot.data!),
                      ProfileTabWidget(snapshot.data!)

                    ],
                  );
                } else
                  return Container();
              }),
        ),
      ),
    );
  }
}
