// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:support/page/feeling/feeling_page.dart';
import 'package:support/page/tabs/practitioners_post_page.dart';
import 'package:support/page/tabs/users_post_page.dart';
import 'package:support/providers/auth_provider.dart';
import 'package:support/utilities/comment_style.dart';

class HomeWidget extends StatefulWidget {
  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(30),
              child: Container(
                height: 30,
                child: TabBar(
                  isScrollable: false,
                  indicatorSize: TabBarIndicatorSize.label,
                  labelStyle: CommentStyle.tabItemTxtStyle(),
                  labelColor: CommentStyle.myThemeColor,
                  unselectedLabelColor: Color(0xFFA9A9A9),
                  tabs: [
                    Stack(
                      alignment: AlignmentDirectional.bottomEnd,
                      children: [
                        Tab(
                          text: 'User Post',
                        ),
                      ],
                    ),
                    Stack(
                      alignment: AlignmentDirectional.bottomStart,
                      children: [
                        Tab(
                          text: 'Practitioner Post',
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.white,
            flexibleSpace: Container(
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 256,
                          height: 35,
                          child: TextField(
                            decoration: CommentStyle.searshTextField(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                      top: 15,
                      right: 10,
                      child: InkWell(
                        onTap: () {

                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => FeelingPage()),
                          );
                        },
                        child: Image.asset('assets/icons/money_icon.png'),
                      )),
                ],
              ),
            ),
          ),
          body: TabBarView(
            children: [UsersPosts(), PractitionerPostPage()],
          ),
        ),
      ),
    );
  }
}
