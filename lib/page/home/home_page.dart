// ignore_for_file: unnecessary_new, deprecated_member_use, prefer_const_constructors

import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:support/model/user.dart';
import 'package:support/page/home/widget/home_widget.dart';
import 'package:support/page/notification/notification_page.dart';
import 'package:support/page/post/create_post_page.dart';
import 'package:support/page/profile/profile_page.dart';
import 'package:support/page/tips/tips_page.dart';
import 'package:support/providers/auth_provider.dart';
import 'package:support/services/local_notification_service.dart';
import 'package:support/utilities/comment_style.dart';

class HomePage extends StatefulWidget {
  static const routeName = 'home_page';





  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final String pagechooser= message['status'];
    Navigator.pushNamed(context, pagechooser);
  }

   late final FirebaseMessaging _firebaseMessaging ;




  bool selectedPosts = false;
  List<Widget> _Pages = [
    HomeWidget(),
    TipsPage(),
    Container(),
    NotificationPage(),
    ProfilePage(),

  ];
   User? user;
  int _selectedIndex = 0;
  late PageController _pageController;

     void getuser()async {
       SharedPreferences pref = await SharedPreferences.getInstance();
       Map<String,dynamic> json = jsonDecode(pref.getString('user').toString());
       user = User.fromJson(json) ;

     }
  @override
  void initState() {
       getuser();
    super.initState();


       var  messaging =  FirebaseMessaging.instance;
       messaging.getInitialMessage();
       messaging.subscribeToTopic("messaging");
       messaging.subscribeToTopic(AuthProvider.user!.id.toString());
       FirebaseMessaging.onMessageOpenedApp.listen((message) {
         print ('clicked message '+message.data['route'].toString());
         Navigator.of(context).pushNamed(message.data['route'].toString());
       });
       FirebaseMessaging.onMessage.listen((message) {
         if(message.data['route'].toString()=='login_page'){
           Provider.of<AuthProvider>(context,listen: false).logout();
         }else{

           LocalNotificationService.display(message);

         }

       });

       _pageController = PageController();
  }

  void _onItemTapped(int index) {
    setState(() {
      if (index == 2) {
        showModalBottomSheet(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            ),
            context: context,
            builder: (context) {
              return toCreatePostWidget(user!.id);
            });
      } else {
        _selectedIndex = index;
      }
    });
  }

  Widget toCreatePostWidget(id) {
    return FutureBuilder<User>(future: User.getUserById(id),
      builder: (BuildContext context,AsyncSnapshot<User> snapshot){
      if(snapshot.hasData){
        return Container(
          height: 160,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Column(
              children: [
                Text(
                  "Create post as ",
                  style: CommentStyle.smallBoldBlackTxtStyle(),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => CreatePostPage('0')),
                        );
                      },
                      child: Container(
                        height: 56,
                        width: 116,
                        decoration: BoxDecoration(
                          color: CommentStyle.myThemeColor,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Center(
                          child: Text(
                            'User',
                            style: CommentStyle.smallBoldWhiteTxtStyle(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    snapshot.data!.practiotionerStatus == '1'? InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => CreatePostPage('1')),
                        );
                      },
                      child: Container(
                        height: 56,
                        width: 116,
                        decoration: BoxDecoration(
                          color: CommentStyle.myThemeColor,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Center(
                          child: Text(
                            'Practitioner',
                            style: CommentStyle.smallBoldWhiteTxtStyle(),
                          ),
                        ),
                      ),
                    ) : Container(),
                  ],
                ),
              ],
            ),
          ),
        );

      }else {

      }
      return Container(height: 160,);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size.width.toString());
    return Scaffold(
      backgroundColor: Colors.white,
      body: _Pages[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        unselectedItemColor: Colors.grey,
        backgroundColor: Colors.white,
        elevation: 3,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: CommentStyle.myThemeColor,
        currentIndex: _selectedIndex,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        onTap: _onItemTapped,
        items: [
          BottomNavigationBarItem(
            icon: new Image.asset('assets/icons/home_icon.png'),
            activeIcon: new Image.asset(
              'assets/icons/home_icon.png',
              color: CommentStyle.myThemeColor,
            ),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: new Image.asset('assets/icons/tips_icon.png'),
            activeIcon: new Image.asset(
              'assets/icons/tips_icon.png',
              color: CommentStyle.myThemeColor,
            ),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: new Image.asset('assets/icons/add_icon.png'),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: new Image.asset('assets/icons/notification_icon.png'),
            activeIcon: new Image.asset(
              'assets/icons/notification_icon.png',
              color: CommentStyle.myThemeColor,
            ),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: new Image.asset('assets/icons/profile_icon.png'),
            activeIcon: new Image.asset(
              'assets/icons/profile_icon.png',
              color: CommentStyle.myThemeColor,
            ),
            title: Text(''),
          )
        ],
      ),
    );
  }
}
