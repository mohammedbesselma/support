import 'package:flutter/material.dart';
import 'package:support/model/slide.dart';
import 'package:support/utilities/comment_style.dart';

class SlideItem extends StatelessWidget {
  final int index;
  SlideItem(this.index);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: MediaQuery.of(context).size.height - 200,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                slideList[index].title,
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Container(
                child: Image.asset(slideList[index].imageUrl),
              ),
            ),
            Text(
              slideList[index].description,
              textAlign: TextAlign.center,
              style: CommentStyle.smallFogyTxtStyle(),
            ),
          ],
        ),
      ),
    );
  }
}
