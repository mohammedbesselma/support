import 'package:flutter/material.dart';
import 'package:support/utilities/comment_style.dart';

class SlideDots extends StatelessWidget {
  bool isActive;
  SlideDots(this.isActive);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: const EdgeInsets.symmetric(horizontal: 10),
      height: isActive ? 7 : 7,
      width: isActive ? 17 : 17,
      decoration: BoxDecoration(
        color: isActive ? CommentStyle.myThemeColor : CommentStyle.fogytex,
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }
}
