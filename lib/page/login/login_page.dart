// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:support/page/register/register_page.dart';
import 'package:support/providers/auth_provider.dart';
import 'package:support/utilities/comment_style.dart';
import 'package:support/page/home/home_page.dart';
import 'package:support/utilities/validate.dart';

class LoginPage extends StatefulWidget {
  static const routeName = 'login_page';
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isChecked = false;
  late String email;
  late String password;

  Future<void> login() async {
    final form = _formKey.currentState;
    if (form!.validate()) {
      Loader.show(context,
          progressIndicator: SpinKitFadingCircle(
            color: Colors.black,
            size: 50.0,
          ));
      await Provider.of<AuthProvider>(context, listen: false)
          .login(email, password)
          .then((response) {
        if (response) Navigator.of(context).pop();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              alignment: AlignmentDirectional.bottomEnd,
              children: [
                Positioned(
                  top: 0,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.5,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/loginbg.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.61,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(45),
                          topRight: Radius.circular(45))),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 76, left: 30, right: 30, bottom: 30),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Welcome back",
                              style: CommentStyle.largBoldGreenTxtStyle(),
                            ),
                            SizedBox(
                              height: 79,
                            ),
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              textCapitalization: TextCapitalization.words,
                              decoration:
                                  CommentStyle.textFieldDecoration('Email'),
                              validator: (value) {
                                email = value!.trim();
                                return Validate.requiredField(
                                    value, 'email is required');
                              },
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            TextFormField(
                              obscureText: true,
                              keyboardType: TextInputType.visiblePassword,
                              decoration:
                                  CommentStyle.textFieldDecoration('Password'),
                              validator: (value) {
                                password = value!.trim();
                                return Validate.requiredField(
                                    value, "password required");
                              },
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Theme(
                                      data: Theme.of(context).copyWith(
                                        unselectedWidgetColor:
                                            CommentStyle.myThemeColor,
                                      ),
                                      child: Checkbox(
                                        activeColor: CommentStyle.myThemeColor,
                                        checkColor: Colors.white,
                                        value: isChecked,
                                        shape: CircleBorder(),
                                        onChanged: (bool? value) {
                                          setState(() {
                                            isChecked = value!;
                                          });
                                        },
                                      ),
                                    ),
                                    Text(
                                      'Remember me',
                                      style: CommentStyle.smallTextStyle(),
                                    )
                                  ],
                                ),
                                InkWell(
                                  onTap: () {},
                                  child: Text(
                                    'Forgot Password',
                                    style: CommentStyle.smallGreenTxtStyle(),
                                  ),
                                )
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Text('Don\'t have account?',
                                        style:
                                            CommentStyle.smallFogyTxtStyle()),
                                    InkWell(
                                      onTap: () {
                                        Navigator.of(context)
                                            .pushNamed(RegisterPage.routeName);
                                      },
                                      child: Text(
                                        'Sign Up',
                                        style: TextStyle(
                                          fontSize: 10,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w500,
                                          color: CommentStyle.myThemeColor,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                InkWell(
                                  onTap: () {
                                    login();
                                  },
                                  child: Image.asset('assets/images/loginicon.png'),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
