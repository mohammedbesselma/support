import 'dart:async';

import 'package:flutter/material.dart';
import 'package:support/model/comment.dart';
import 'package:support/model/post.dart';
import 'package:support/page/post/comment_item_widget.dart';
import 'package:provider/provider.dart';

class CommentsListWidget extends StatefulWidget {
  late Post post;

  CommentsListWidget(this.post);

  @override
  _CommentsListWidgetState createState() => _CommentsListWidgetState();
}

class _CommentsListWidgetState extends State<CommentsListWidget> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Comment>>(
        future: Provider.of<Comment>(context, listen: true)
            .getComments(widget.post.id!),
        builder: (BuildContext context, AsyncSnapshot<List<Comment>> snapshot) {

              if (snapshot.hasData) {

                return ListView.builder(

                        padding: EdgeInsets.all(8),
                        itemCount: snapshot.data!.length,
                        itemBuilder: (BuildContext ctxt, int index){
                          if(snapshot.data![index].replyTo == 'null' ){
                            return CommentItemWidget(
                                comment: snapshot.data![index],comments: snapshot.data,postUserId: widget.post.userId!);
                          }else return Container();

                        }
                            );
              } else {
                return Container();
              }
          }
        );
  }
}
