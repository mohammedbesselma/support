
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:support/model/comment.dart';
import 'package:support/model/user.dart';
import 'package:support/services/show_comment_menu.dart';
import 'package:support/utilities/comment_style.dart';

class CommentItemWidget extends StatelessWidget {
    Comment comment;
    List<Comment>? comments ;
    Function? setReply ;
    String? postUserId;



  CommentItemWidget({required this.comment,this.comments,this.setReply,this.postUserId});


  @override
  Widget build(BuildContext context) {
    comments!.forEach((com) {
      if(com.replyTo == comment.id){
        comment.replyList.add(com);
      }
    });
    double posx = 100.0;
    double posy = 100.0;


   // List<Comment> strings = comments.where((element) => element.replyTo == comment.id).toList();
    //strings.removeWhere((item) => item.id == comment.id);
    //print(strings.length.toString());
    return StatefulBuilder(builder: (BuildContext context ,StateSetter setState) {

      void onTapDown(TapDownDetails details) {

        var x = details.globalPosition.dx;
        var y = details.globalPosition.dy;
        // or user the local position method to get the offset
        setState((){
          posy = y;
          posx = x;
        });
      }
      return  Container(
        child: Padding(
          padding: EdgeInsets.all(12),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row( crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(comment.username!,style: CommentStyle.small13BoldGreenTxtStyle(),),
                  SizedBox(width: 5,),
                  Text(CommentStyle.getTextFromDate(comment.createdAt!),style: CommentStyle.smallFogyTxtStyle(),)
                ],),
              Padding(padding: EdgeInsets.symmetric(vertical: 10),

                child: Text(comment.content!,style: CommentStyle.commmentTextStyle(),),

              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    width: 10,
                    height: 20,
                    child: InkWell(
                      onTapDown: (TapDownDetails details) => onTapDown(details),
                      onTap: (){
                        showDialog<void>(
                          barrierColor: Colors.transparent,
                          barrierDismissible: true,
                          context: context,
                          builder: (BuildContext context) {
                            return CommentMenu(comment,posy,posx);
                          },
                        );
                      },
                      child: Image.asset('assets/icons/dots_icon.png',color: Color(0xFF545454),),
                    ),

                  ),
                  SizedBox(width: 40,),
                  Row(children: [
                    Icon(Icons.reply,color: Color(0xFF545454),size: 20,),
                    SizedBox(width: 5,),
                    Text('Reply',style: CommentStyle.commmentTextStyle(),)
                  ],),
                  SizedBox(width: 40,),
                  Row(children: [
                    Icon(Icons.favorite_outlined,color: Color(0xFF545454),size: 20,),
                    SizedBox(width: 5,),
                    Text('1.1k',style: CommentStyle.small13BoldGreenTxtStyle(),)
                  ],),



                ],
              ),
              SizedBox(height: 20,),
              getTextWidgets(comment,comments)
            ],
          ),
        ),
      );
    }

    );
  }
}


Widget getTextWidgets(Comment comment,comments)

{
    return Padding(
      padding: const EdgeInsets.only(left: 20,top: 5),
      child: new Column(children: comment.replyList.map((item) => new CommentItemWidget(comment: item,comments: comments,)).toList()));


}

