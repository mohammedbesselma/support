import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:support/model/comment.dart';
import 'package:support/model/post.dart';
import 'package:support/model/user.dart';
import 'package:support/page/home/home_page.dart';
import 'package:support/page/post/comments_list_widget.dart';
import 'package:support/providers/comments_provider.dart';
import 'package:support/services/show_post_menu.dart';
import 'package:support/services/toast_service.dart';
import 'package:support/utilities/comment_style.dart';
import 'package:support/utilities/validate.dart';
import 'package:support/widgets/post/comment_widget.dart';
import 'package:support/widgets/post/like_widget.dart';
import 'package:support/widgets/post/saving_widget.dart';

class PostDetaisPage extends StatefulWidget {
  static const routeName = 'post_details_page';

  PostDetaisPage({
    post,
    comments,
  });

  @override
  _PostDetaisPageState createState() => _PostDetaisPageState();
}

class _PostDetaisPageState extends State<PostDetaisPage> {
  late User user;
  final fieldText = TextEditingController();
  Post? post;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late String commentvalur;

  Future<void> _createComment(Post post, comment) async {
    final form = _formKey.currentState;
    if (form!.validate()) {
      Map<String, String> data = {
        'to_user': post.userId.toString(),
        'content': comment.toString(),
        'post_id': post.id.toString(),
        'user_id': user.id.toString()
      };
      Loader.show(context,
          progressIndicator: SpinKitFadingCircle(
            color: Colors.black,
            size: 80.0,
          ));
      bool response = await Provider.of<Comment>(context, listen: false)
          .createComment(data);

      if (response) {
        ToastService.showSuccessToast('comment added');
        Loader.hide();
      }
    }
  }

  double posx = 100.0;
  double posy = 100.0;

  void onTapDown(TapDownDetails details, StateSetter localsetter) {
    var x = details.globalPosition.dx;
    var y = details.globalPosition.dy;
    // or user the local position method to get the offset
    print(details.localPosition);
    print("tap down " + x.toString() + ", " + y.toString());
    localsetter(() {
      posy = y;
      posx = x;
    });
  }

  @override
  Widget build(BuildContext context) {
    final post = ModalRoute.of(context)!.settings.arguments as Post;

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: CommentStyle.homeBackgroundColor,
          leading: Container(
              child: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: CommentStyle.myThemeColor,
            ),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(HomePage.routeName);
            },
          )),
          actions: [
            SizedBox(
              width: 20,
            ),
            SavingWidget(post.id.toString()),
            StatefulBuilder(builder:
                (BuildContext context, StateSetter local) {
              return Container(
                width: 30,
                height: 30,
                child: InkWell(
                  onTapDown:
                      (TapDownDetails details) =>
                      onTapDown(details, local),
                  onTap: () {
                    showDialog<void>(
                      barrierColor: Colors.transparent,
                      barrierDismissible: true,
                      context: context,
                      builder: (BuildContext context) {
                        return PostMenu(post,posy,posx);
                      },
                    );
                  },
                  child:
                  Image.asset('assets/icons/dots_icon.png'),
                ),
              );
            }),
          ],
        ),
        body: FutureBuilder<Post>(
          future: Provider.of<Post>(context, listen: true).getPostById(post.id),
          builder: (BuildContext context, AsyncSnapshot<Post> snapshot) {
            if (snapshot.hasData) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    color: Colors.white,
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 20, left: 2, right: 2, bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      snapshot.data!.username!,
                                      style:
                                          CommentStyle.smallBoldGreenTxtStyle(),
                                    ),
                                    Text(
                                      CommentStyle.getTextFromDate(
                                          post.createdAt!),
                                      style:
                                          CommentStyle.extraSmallFogyTxtStyle(),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Text(
                            snapshot.data!.content!,
                            style: CommentStyle.smallTextStyle(),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  LikeWidget(snapshot.data!),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  CommentWidget(
                                      snapshot.data!.comments!.length),
                                ],
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.share,
                                    size: 20,
                                    color: CommentStyle.myThemeColor,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Recommend',
                                    style: CommentStyle.smallTextStyle(),
                                  )
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 30,
                    width: MediaQuery.of(context).size.width,
                    color: CommentStyle.homeBackgroundColor,
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Text(
                            'New Comments',
                            style: CommentStyle.smallTextStyle(),
                          ),
                          Icon(
                            Icons.keyboard_arrow_down,
                            size: 20,
                          )
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                      child: CommentsListWidget(snapshot.data!)),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          Image.asset('assets/icons/profile.png'),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            height: 36,
                            width: 250,
                            child: Form(
                              key: _formKey,
                              child: TextFormField(
                                  style: TextStyle(
                                    fontFamily: "Roboto",
                                    fontSize: 12.0,
                                    color: Colors.black87,
                                    height: 0.7,
                                  ),
                                  controller: fieldText,
                                  keyboardType: TextInputType.text,
                                  decoration:
                                      CommentStyle.commentTextFieldStyle(),
                                  validator: (value) {
                                    commentvalur = value!.trim();
                                    return Validate.requiredField(
                                        value, 'Name is required.');
                                  }),
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                final form = _formKey.currentState;
                                if (form!.validate()) {
                                  _createComment(post, commentvalur);
                                }
                              },
                              icon: Icon(Icons.play_arrow))
                        ],
                      ),
                    ),
                    color: CommentStyle.homeBackgroundColor,
                  ),
                ],
              );
            } else
              return Container();
          },
        ),
      ),
    );
  }

  @override
  initState() {
    getuser();
  }

  Future<void> getuser() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map<String, dynamic> json = jsonDecode(pref.getString('user').toString());

    user = User.fromJson(json);
  }
}
