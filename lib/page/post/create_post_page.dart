import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:record/record.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:support/model/catrgory.dart';
import 'package:support/model/post.dart';
import 'package:support/model/user.dart';
import 'package:support/page/tabs/widget/category_item_widget.dart';
import 'package:support/services/audio.dart';
import 'package:support/services/file_viewer_service.dart';
import 'package:support/services/toast_service.dart';
import 'package:support/services/video_player.dart';
import 'package:support/utilities/comment_style.dart';
import 'package:support/utilities/validate.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_indicators/progress_indicators.dart';

class CreatePostPage extends StatefulWidget {
  static const routeName = 'create_post_page';

  Post? post;

  String byPractitioner;
  CreatePostPage(this.byPractitioner,{this.post});

  @override
  _CreatePostPageState createState() => _CreatePostPageState();
}

class _CreatePostPageState extends State<CreatePostPage> {
  late String content;
  late String? ext;
  late String? title;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late File? _file = null;
  late String byParactitioner = widget.byPractitioner;
  bool _isRecording = false;
  String selectedCategoryTitle = 'selecte category';
  final _audioRecorder = Record();
  int selectedCategory = 0;

  Future<void> _start() async {
    ext = 'm4a';
    try {
      if (await _audioRecorder.hasPermission()) {
        await _audioRecorder.start();

        bool isRecording = await _audioRecorder.isRecording();
        setState(() {
          _isRecording = isRecording;
        });

      }
    } catch (e) {
      print(e); 
    }
  }

  Future<void> _stop() async {
    var path = await _audioRecorder.stop();
    _file = File(path!);

    //*widget.onStop(path!);

    setState(() => _isRecording = false);
  }

  Future<Null> _pickImage() async {
    ext = 'mp4';
    ImagePicker _picker = ImagePicker();
    ImageSource source = ImageSource.gallery;
    final pickedVideo = await _picker.pickVideo(source: source);
    setState(() {
      _file = File(pickedVideo!.path);
    });
  }

  Future<void> _createPost(userId) async {
    final form = _formKey.currentState;

    if (form!.validate()) {
      Loader.show(context,
          progressIndicator: SpinKitFadingCircle(
            color: Colors.black,
            size: 80.0,
          ));
      if (_file != null) {
        Map<String, String> data = {
          'content': content,
          'userid': userId,
          'ext': ext!,
          'post_title': title!,
          'by_practitioner': byParactitioner,
        };

        bool response = await Provider.of<Post>(context, listen: false)
            .createPost(data, file: _file!.path);

        if (response) {
          Navigator.of(context).pop();
          Loader.hide();
          ToastService.showSuccessToast('post is saved successfully');
        } else {
          Loader.hide();
        }
      } else {
        Map<String, String> data = {
          'content': content,
          'userid': userId,
          'category_id': selectedCategory.toString(),
          'by_practitioner': byParactitioner,
        };

        bool response =
            await Provider.of<Post>(context, listen: false).createPost(data);

        if (response) {
          Navigator.of(context).pop();
          Loader.hide();
          ToastService.showSuccessToast('post created successfully');
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: FutureBuilder<User>(
            future: Provider.of<User>(context,listen: true).getUserData(),
            builder: (BuildContext context,AsyncSnapshot<User> snapshot){

              if(snapshot.hasData){
                if(widget.post != null && widget.post!.categoryId != 'null'){

                  selectedCategory = int.parse(widget.post!.categoryId!);
                }
                return SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                IconButton(
                                  icon: Icon(Icons.close),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                FlatButton(
                                  onPressed: () {
                                    _createPost(snapshot.data!.id);
                                  },
                                  child: Container(
                                    height: 35,
                                    width: 70,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      color: CommentStyle.myThemeColor,
                                    ),
                                    child: Center(
                                      child: Text(
                                         widget.post == null ?'Submit' : 'Update',
                                        style: CommentStyle.smallBoldWhiteTxtStyle(),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            byParactitioner == '1'
                                ? Padding(
                              padding: EdgeInsets.all(8),
                              child: TextFormField(
                                initialValue: widget.post != null ? widget.post!.postTitle: '' ,
                                decoration:
                                CommentStyle.addPostTextField('Title'),
                                keyboardType: TextInputType.multiline,
                                minLines:
                                1, //Normal textInputField will be displayed
                                maxLines: null,
                                validator: (value) {
                                  title = value!.trim();
                                  return Validate.requiredField(
                                      value, 'title is required.');
                                }, // when user presses enter
                              ),
                            )
                                : Padding(
                              padding: const EdgeInsets.all(20),
                              child: InkWell(
                                onTap: () {
                                  showModalBottomSheet(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20),
                                            topRight: Radius.circular(20)),
                                      ),
                                      context: context,
                                      builder: (context) {
                                        return CategorySelector();
                                      });
                                },
                                child: Row(
                                  children: [
                                    Text(
                                      widget.post!=null? catigories[int.parse(widget.post!.categoryId!)].categoryText.toString():selectedCategoryTitle ,
                                      style:
                                      CommentStyle.smallBoldBlackTxtStyle(),
                                    ),
                                    Icon(
                                      Icons.keyboard_arrow_down,
                                      size: 20,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      decoration: CommentStyle.addPostTextField(
                                          'Tyoe that you want to post..'),
                                      keyboardType: TextInputType.multiline,
                                      minLines:
                                      1, //Normal textInputField will be displayed
                                      maxLines: null,
                                      validator: (value) {
                                        content = value!.trim();

                                        return Validate.requiredField(
                                            value, 'content is required.');
                                      }, // when user presses enter
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        _isRecording
                            ? Container(
                          child: Column(
                            children: [
                              JumpingDotsProgressIndicator(
                                fontSize: 50.0,
                                color: CommentStyle.myThemeColor,
                              ),
                              IconButton(onPressed: (){
                                _stop();
                              },
                                  icon: Icon(Icons.stop,color: Colors.red,size: 30,))
                            ],
                          ),
                        )
                            : Container(),
                        _file != null ? Container(

                            child: Column(
                              children: [
                                IconButton(
                                  onPressed: () {
                                    setState(() {
                                      ext = null;
                                      _file = null;
                                    });
                                  },
                                  icon: Icon(
                                    Icons.close,
                                    color: Colors.red,
                                    size: 30,
                                  ),
                                ),
                                FileViewer(ext!,file: _file,),
                              ],
                            )) : Container(),
                        Row(
                          children: [
                            IconButton(
                              icon: Image.asset('assets/icons/imagepicker.png'),
                              onPressed: () {
                                _pickImage();
                              },
                            ),
                            Container(
                              height: 40,
                              width: 40,
                              child: GestureDetector(
                                  onTap: () {
                                    _start();
                                  },
                                  child: Container(
                                      height: 40,
                                      width: 40,
                                      child: Image.asset(
                                          'assets/icons/audiorecorder.png'))),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              }else  return Center(
                child: Container(
                  height: 30,
                  width: 30,
                  child: CircularProgressIndicator(),
                ),
              );
          },),
        ));
  }



  Widget CategorySelector() {
    return StatefulBuilder(
        builder: (BuildContext context, StateSetter localSetState) {
      return Container(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Container(
                height: 6,
                width: 41,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3),
                    color: Color(0xFFCCCCCC)),
              ),
              Row(
                children: [
                  Text(
                    'Categories',
                    style: CommentStyle.smallBoldGreenTxtStyle(),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 1,
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xFFA9A9A9)),
              ),
              ListView.builder(
                  shrinkWrap: true,
                  itemCount: catigories.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: () {
                        localSetState(() {
                          catigories
                              .forEach((element) => element.isSelected = false);
                          catigories[index].isSelected = true;
                          Navigator.of(context).pop();
                        });
                        setState(() {
                          selectedCategoryTitle =
                              catigories[index].categoryText;
                          selectedCategory = catigories[index].id;
                        });
                      },
                      child: CategoryItem(catigories[index]),
                    );
                  })
            ],
          ),
        ),
      );
    });
  }
}
