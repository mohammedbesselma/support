// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:support/page/login/login_page.dart';
import 'package:support/providers/auth_provider.dart';
import 'package:support/utilities/comment_style.dart';
import 'package:support/utilities/validate.dart';

class RegisterPage extends StatefulWidget {
  static const routeName = 'register_page';
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isChecked = false;
  double itemspace = 5;
  late String name;
  late String email;
  late String password;
  late String phone;
  Future<void> register() async {
    final form = _formKey.currentState;

    if (form!.validate()) {
      Loader.show(context,
          progressIndicator: SpinKitFadingCircle(
            color: Colors.black,
            size: 50.0,
          ));

      await Provider.of<AuthProvider>(context, listen: false)
          .register(name, email, password, phone)
          .then((response) {
        if (response) {
          Navigator.of(context).pop();
          Navigator.of(context).pop();
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              alignment: AlignmentDirectional.bottomEnd,
              children: [
                Positioned(
                  top: 0,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.5,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/loginbg.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.61,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(45),
                          topRight: Radius.circular(45))),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 20, left: 20, right: 20, bottom: 30),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Get Started",
                              style: CommentStyle.largBoldGreenTxtStyle(),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              keyboardType: TextInputType.text,
                              decoration:
                                  CommentStyle.textFieldDecoration('Full name'),
                              validator: (value) {
                                name = value!.trim();
                                return Validate.requiredField(
                                    value, "name required");
                              },
                            ),
                            SizedBox(
                              height: itemspace,
                            ),
                            SizedBox(
                              height: itemspace,
                            ),
                            TextFormField(
                              keyboardType: TextInputType.text,
                              decoration:
                                  CommentStyle.textFieldDecoration('Email'),
                              validator: (value) {
                                email = value!.trim();
                                return Validate.requiredField(
                                    value, "email required");
                              },
                            ),
                            SizedBox(
                              height: itemspace,
                            ),
                            TextFormField(
                              keyboardType: TextInputType.text,
                              decoration:
                                  CommentStyle.textFieldDecoration('password'),
                              validator: (value) {
                                password = value!.trim();
                                return Validate.requiredField(
                                    value, "password required");
                              },
                            ),
                            SizedBox(
                              height: itemspace,
                            ),
                            TextFormField(
                              keyboardType: TextInputType.text,
                              decoration: CommentStyle.textFieldDecoration(
                                  're-password'),
                              validator: (value) {
                                password = value!.trim();
                                return Validate.requiredField(
                                    value, "not confirmed password");
                              },
                            ),
                            SizedBox(
                              height: itemspace,
                            ),
                            TextFormField(
                              keyboardType: TextInputType.phone,
                              decoration:
                                  CommentStyle.textFieldDecoration('phone'),
                              validator: (value) {
                                phone = value!.trim();
                                return Validate.requiredField(
                                    value, "phone required");
                              },
                            ),
                            SizedBox(
                              height: itemspace,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Theme(
                                      data: Theme.of(context).copyWith(
                                        unselectedWidgetColor:
                                            CommentStyle.myThemeColor,
                                      ),
                                      child: Checkbox(
                                        activeColor: CommentStyle.myThemeColor,
                                        checkColor: Colors.white,
                                        value: isChecked,
                                        shape: CircleBorder(),
                                        onChanged: (bool? value) {
                                          setState(() {
                                            isChecked = value!;
                                          });
                                        },
                                      ),
                                    ),
                                    Text(
                                      'I agree to the',
                                      style: CommentStyle.smallFogyTxtStyle(),
                                    ),
                                    SizedBox(
                                      width: 1,
                                    ),
                                    InkWell(
                                      onTap: () {},
                                      child: Text(
                                        'Terms of services',
                                        style:
                                            CommentStyle.smallGreenTxtStyle(),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 1,
                                    ),
                                    Text(
                                      'and',
                                      style: CommentStyle.smallFogyTxtStyle(),
                                    ),
                                    SizedBox(
                                      width: 1,
                                    ),
                                    InkWell(
                                      onTap: () {},
                                      child: Text(
                                        'Privacy Policy',
                                        style:
                                            CommentStyle.smallGreenTxtStyle(),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Text('have an account?',
                                        style:
                                            CommentStyle.smallFogyTxtStyle()),
                                    InkWell(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Text(
                                        'Sign In',
                                        style: TextStyle(
                                          fontSize: 10,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w500,
                                          color: CommentStyle.myThemeColor,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                InkWell(
                                  onTap: () {
                                    register();
                                  },
                                  child: Image.asset('assets/images/loginicon.png'),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
