import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:support/model/user.dart';
import 'dart:convert';
import 'package:support/services/api_http.dart';
import 'package:support/services/toast_service.dart';

enum Status {
  Uninitialized,
  Authenticated,
  Unauthenticated,
}

class AuthProvider with ChangeNotifier {
  Status _status = Status.Uninitialized;
  static  User? user;
  static String version = 'api';
  late String host = Network.host;
  late String _token;
  int _timeoutDuration = 15;

  AuthProvider() {
    initAuthProvider();
  }

  initAuthProvider() async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    String? token = storage.getString('access_token');
    Map<String,dynamic> json = jsonDecode(storage.getString('user').toString());

    if (token != null) {
      user = User.fromJson(json) ;
      _token = token;
      _status = Status.Authenticated;
    } else {
      _status = Status.Unauthenticated;
    }
    notifyListeners();
  }

  Status get status => _status;

  void logout() async {
    FirebaseMessaging.instance.deleteToken();
    SharedPreferences storage = await SharedPreferences.getInstance();
    storage.clear();

    _status = Status.Unauthenticated;

    notifyListeners();
  }

  Future<bool> register(
      String name, String email, String password, String phone) async {
    final url = host + version + "/register";

    var data = {
      'name': name,
      'email': email,
      'phone': phone,
      'password': password
    };
    try {
      final response = await http
          .post(Uri.parse(url), body: jsonEncode(data), headers: _setHeaders())
          .timeout(
        Duration(seconds: _timeoutDuration),
        onTimeout: () {
          Loader.hide();
          ToastService.showErrorToast('Please Check Your Internet Connection');
          throw Future.error('');

        },
      );
      if (response != null && response.statusCode == 200) {
        await storeUserData(response.body);
        Loader.hide();
        _status = Status.Authenticated;
        notifyListeners();

        return true;
      }
      if (response != null && response.statusCode == 400) {
        Map data = json.decode(response.body);
        Loader.hide();
        ToastService.showErrorToast(data['message']);
        return false;
      }
    }catch (e){

      ToastService.showErrorToast('Something Went Wrong, Please Try Later');
      Loader.hide();
      return false;

    }
    return false;


  }

  _setHeaders() =>
      {'Content-type': 'application/json', 'Accept': 'application/json'};

  Future<bool> login(String email, String password) async {
    final url = host + version + "/login";
    var data = {
      'email': email,
      'password': password,
      'remember-me': true,
    };
    try{

      final response = await http
          .post(Uri.parse(url), body: jsonEncode(data),headers: _setHeaders())
          .timeout(
        Duration(seconds: _timeoutDuration),
        onTimeout: () {
          Loader.hide();
          ToastService.showErrorToast('Please Check Your Internet Connection');
          throw Future.error('');
        },
      );
      print(response.body);
      print(response.statusCode);
      if (response != null && response.statusCode == 200) {
        final result = json.decode(response.body);
        await storeUserData(response.body);
        _status = Status.Authenticated;
        Loader.hide();
        notifyListeners();
        return true;
      }
      if (response != null && response.statusCode == 400) {
        Map data = json.decode(response.body);
        Loader.hide();
        ToastService.showErrorToast(data['message']);

        return false;
      }

      ToastService.showErrorToast('Something Went Wrong, Please Try Later');
      Loader.hide();
      return false;

    }catch (e){
      ToastService.showErrorToast('Server not found');
      Loader.hide();
      return false ;
    }

  }

  storeUserData(String response) async {
    var data = json.decode(response);
    SharedPreferences storage = await SharedPreferences.getInstance();
    await storage.setString('access_token', data['access_token']);
    print(data['user']);
    await storage.setString('user', json.encode(data['user']));

    Map<String,dynamic> json1 = jsonDecode(storage.getString('user').toString());
    user = User.fromJson(json1) ;
  }
}
