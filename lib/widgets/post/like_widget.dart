import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:support/model/like.dart';
import 'package:support/model/post.dart';
import 'package:support/model/user.dart';
import 'package:support/utilities/comment_style.dart';

class LikeWidget extends StatefulWidget {
  Post post;
  LikeWidget(this.post);

  @override
  _LikeWidgetState createState() => _LikeWidgetState();
}

class _LikeWidgetState extends State<LikeWidget> {

   late bool isLiked = false ;
  late String id;
  late int likes = widget.post.likes!.length ;
  bool isLoading = false;



  Future<void> _deletelike(StateSetter localStateSetter,id,userid) async {

    localStateSetter((){
      isLoading = !isLoading;

    });

    var data = {
      'id': id,
      'user_id' : userid.toString(),
      'post_id' : widget.post.id,
      'to_user' : widget.post.userId


    };

    bool response =
        await Provider.of<Like>(context, listen: false).deleteLike(data);

    if (!response){
      localStateSetter((){
        isLoading = !isLoading;
        isLiked = !isLiked;
      });
    }else {



      localStateSetter((){
        likes--;
        isLoading = !isLoading;
        isLiked = false ;


      });

    }


  }

  Future<void> _createlike(StateSetter localStateSetter,userid) async {


      localStateSetter((){
        isLoading = !isLoading;

      });

    var data = {
      'user_id': userid,
      'post_id': widget.post.id.toString(),
      'to_user' : widget.post.userId

    };

    int response =
        await Provider.of<Like>(context, listen: false).createLike(data);

      if (response == -1) {
        localStateSetter(() {
          isLoading = !isLoading;

        });
      }else {
        localStateSetter((){
          id = response.toString();
          likes++;
          isLoading = !isLoading;
          isLiked = true;
        });


      }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    return FutureBuilder<User>(
      future: Provider.of<User>(context,listen: false).getUserData(),
      builder: (BuildContext context,AsyncSnapshot<User>snapshot){
      if(snapshot.hasData){

        widget.post.likes!.forEach((element) {
          if (snapshot.data!.id == element.userId) {
            isLiked = !isLiked;
            id = element.id.toString();
          }
        });
        return StatefulBuilder(
            builder: (BuildContext context, StateSetter localStateSetter) {
              return Container(
                child: Row(
                  children: [
                    Container(
                      width: 30,
                      child: InkWell(
                        onTap: () {

                          if (!isLoading){
                            if (isLiked) {
                              _deletelike(localStateSetter,id,snapshot.data!.id);
                            } else {
                              _createlike(localStateSetter,snapshot.data!.id);
                            }

                          }


                        },
                        child: Icon(isLiked
                            ?
                        Icons.favorite:Icons.favorite_border,
                          size: 25,
                          color: Colors.red,
                        )
                      ),
                    ),
                    Text(
                      likes.toString(),
                      style: CommentStyle.smallTextStyle(),
                    )
                  ],
                ),
              );
            });
      }
      else return Container();
    },);
  }
}
