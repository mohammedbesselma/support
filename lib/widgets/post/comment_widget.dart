import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:support/model/comment.dart';
import 'package:support/utilities/comment_style.dart';

class CommentWidget extends StatefulWidget {
  int comments;

  CommentWidget(this.comments);

  @override
  _CommentWidgetState createState() => _CommentWidgetState();
}

class _CommentWidgetState extends State<CommentWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            width: 30,
            child: InkWell(
              child: Image.asset('assets/icons/comment_icon.png'),
            ),
          ),
          Text(
            widget.comments.toString(),
            style: CommentStyle.smallTextStyle(),
          )
        ],
      ),
    );
  }
}
