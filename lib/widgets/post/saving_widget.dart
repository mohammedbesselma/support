import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:support/model/like.dart';
import 'package:support/model/saving.dart';
import 'package:support/model/user.dart';
import 'package:support/utilities/comment_style.dart';

class SavingWidget extends StatefulWidget {
  late String postId;

  SavingWidget(this.postId);

  @override
  _SavingWidgetState createState() => _SavingWidgetState();
}

class _SavingWidgetState extends State<SavingWidget> {
  bool isSaved = false;
  late User user;
  late String id;
  bool isLoading = false;

  _getuser() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map<String, dynamic> json = jsonDecode(pref.getString('user').toString());
    user = User.fromJson(json);
  }

  Future<void> _deleteSaving(id, StateSetter localStateSatter) async {
    localStateSatter(() {
      isLoading = !isLoading;
      isSaved = !isSaved;
    });
    var data = {
      'id': id,
    };

    bool response =
        await Provider.of<Saving>(context, listen: false).deleteSaving(data);

    if (!response) {
      localStateSatter(() {
        isLoading = !isLoading;
        isSaved = !isSaved;
      });
    } else {
      localStateSatter(() {
        isLoading = !isLoading;
      });
    }
  }

  Future<void> _createSaving(StateSetter localStateSetter) async {
    localStateSetter(() {
      isLoading = !isLoading;
      isSaved = !isSaved;
    });

    var data = {
      'user_id': user.id.toString(),
      'post_id': widget.postId.toString()
    };

    int response =
        await Provider.of<Saving>(context, listen: false).createSaving(data);

    if (response == -1) {
      localStateSetter(() {
        isLoading = !isLoading;
        isSaved = !isSaved;
      });
    } else {
      localStateSetter(() {
        id = response.toString();
        isLoading = !isLoading;
      });
    }
  }

  @override
  void initState() {
    _getuser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Saving>>(
        future:
            Provider.of<Saving>(context, listen: true).getSaving(widget.postId),
        builder: (BuildContext cnx, AsyncSnapshot<List<Saving>> snapshot) {
          if (snapshot.hasData) {
            snapshot.data!.forEach((element) {
              if (user.id == element.userId) {
                isSaved = !isSaved;
                id = element.id.toString();
              }
            });

            return StatefulBuilder(
                builder: (BuildContext context, StateSetter localStateSetter) {
              return Container(
                width: 30,
                child: InkWell(
                  onTap: () {
                    if (!isLoading) {
                      if (isSaved) {
                        _deleteSaving(id, localStateSetter);
                      } else {
                        _createSaving(localStateSetter);
                      }
                    }
                  },
                  child: isSaved
                      ? Icon(
                          Icons.bookmark,
                          size: 20,
                          color: CommentStyle.myThemeColor,
                        )
                      : Icon(
                          Icons.bookmark_border,
                          size: 20,
                          color: CommentStyle.myThemeColor,
                        ),
                ),
              );
            });
          } else {
            return Container();
          }
        });
  }
}
