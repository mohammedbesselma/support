import 'package:flutter/material.dart';

class Category {
  int id;
  late bool isSelected;
  late String categoryText;
  late Duration duration;
  late Color color ;

  Category(this.id, this.isSelected, this.categoryText,this.duration,this.color);



  static List<Category> getCategory () {
    List<Category> catigories = [
      Category(0, true, 'All category',Duration(  seconds: 30),Colors.yellow),
      Category(1, false, 'Anxiety',Duration(  seconds: 30),Colors.yellow),
      Category(2, false, 'Anger',Duration(minutes:3),Colors.redAccent),
      Category(3, false, 'Irritation',Duration(minutes: 3),Colors.pinkAccent),
      Category(4, false, 'Sadness',Duration(minutes: 3),Colors.orangeAccent),
      Category(5, false, 'Fear',Duration(minutes: 3),Colors.blueGrey),
      Category(6, false, 'Worry',Duration(minutes: 4),Colors.brown),
    ];
    return catigories;
  }
}

final catigories = [
  Category(1, false, 'Anxiety',Duration(  seconds: 30),Colors.yellow),
  Category(2, false, 'Anger',Duration(minutes:3),Colors.redAccent),
  Category(3, false, 'Irritation',Duration(minutes: 3),Colors.pinkAccent),
  Category(4, false, 'Sadness',Duration(minutes: 3),Colors.orangeAccent),
  Category(5, false, 'Fear',Duration(minutes: 3),Colors.blueGrey),
  Category(6, false, 'Worry',Duration(minutes: 4),Colors.brown),
];