import 'package:flutter/material.dart';
class FeelingCategorie with ChangeNotifier{

  int? id;
  late bool? isSelected;
  late String? categoryText;

  FeelingCategorie({this.id, this.isSelected, this.categoryText});

  static  int select = 0;

   static final feelingcatigories = [
    FeelingCategorie(id:1, isSelected: false, categoryText: 'Sade'),
    FeelingCategorie(id :2, isSelected:false, categoryText:'Anxious'),
     FeelingCategorie(id :3, isSelected:false, categoryText:'Exhausted'),
     FeelingCategorie(id :4, isSelected:false, categoryText:'Hopelessness'),
    FeelingCategorie(id :5, isSelected:false, categoryText:'Happy'),
    FeelingCategorie(id :6, isSelected:false, categoryText:'Fatigue'),
    FeelingCategorie(id :7, isSelected:false, categoryText:'Restless'),
    FeelingCategorie(id :8, isSelected:false, categoryText:'Good'),
    FeelingCategorie(id :9, isSelected:false, categoryText:'Diggiculty concentrating'),
    FeelingCategorie(id :10,isSelected: false,categoryText: 'Loss my appetite'),
    FeelingCategorie(id :11,isSelected: false,categoryText: 'Sick'),
    FeelingCategorie(id :12,isSelected: false,categoryText: 'Irritated'),
    FeelingCategorie(id :13,isSelected: false,categoryText: 'Guilt'),
    FeelingCategorie(id :14,isSelected: false,categoryText: 'Helplessness'),
    FeelingCategorie(id :15,isSelected: false,categoryText: 'Other'),

  ];

  Future<List<FeelingCategorie>> getFeelingCategories ()async {
    return feelingcatigories;
  }

  Future<void> selecteCategorie (id)async{

    feelingcatigories.forEach((element) {
      if (element.id == id){
        element.isSelected = true;
        select = id;
      }else {
        element.isSelected = false;
      }
    });
    notifyListeners();

  }

}

