import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:support/services/api_http.dart';

class PractitionerPost {
  late String id, postTitle, createdBy, createdAt, content, mediaLink;

  PractitionerPost(
      {required this.id,
      required this.postTitle,
      required this.createdBy,
      required this.createdAt,
      required this.content,
      required this.mediaLink});

  static Future<List<PractitionerPost>> getPractitionerPosts({context}) async {
    var data = await Network().get('practitionerpost.php');
    print(data.body);

    var postObjsJson = jsonDecode(data.body)['post'] as List;
    List<PractitionerPost> posts = postObjsJson
        .map((tagJson) => PractitionerPost.fromJson(tagJson, data))
        .toList();

    return posts;
  }

  factory PractitionerPost.fromJson(dynamic parsedJson, data) {
    return PractitionerPost(
      id: parsedJson['id'] ?? "",
      postTitle: parsedJson['title'] ?? "",
      createdBy: parsedJson['created_by'] ?? "",
      createdAt: parsedJson['created_at'] ?? "",
      content: parsedJson['content'] ?? "",
      mediaLink: parsedJson['link'] ?? "",
    );
  }
}
