import 'dart:convert';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:support/services/api_http.dart';
import 'package:support/services/toast_service.dart';

class Comment with ChangeNotifier {
  String? id, replyTo, postId;
  String? createdBy, createdAt, content;
  String? username;
  List<Comment> replyList = [];

  Comment(
      {this.id,
      this.replyTo,
      this.createdBy,
      this.createdAt,
      this.content,
      this.postId,
      this.username});

  Future<List<Comment>> getComments(String postId) async {
    var data = {
      'post_id': postId,
    };
    final response = await Network().post('comment', data);
    print(response.body);
    var postObjsJson = jsonDecode(response.body)['comments'] as List;
    List<Comment> posts =
        postObjsJson.map((tagJson) => Comment.fromJson(tagJson)).toList();

    print(posts.length);
    return posts;
  }

  Future<bool> createComment(data) async {
    Network api = Network();

    http.Response response = await api.post('createcomment', data).timeout(Duration(seconds: 10), onTimeout: () {
      ToastService.showErrorToast('Server error');
      return http.Response('Error', 500);
    });

    if (response.statusCode == 200) {
      notifyListeners();
      return true;
    } else {
      Loader.hide();
      ToastService.showErrorToast(
          json.decode(response.body)['message'].toString());

      return false;
    }
  }
  Future<bool> deleteComment(data) async {
    print(data.toString());
    Network api = Network();

    http.Response response = await api.post('deletecomment', data).timeout(Duration(seconds: 10), onTimeout: () {
      ToastService.showErrorToast('Server error');
      return http.Response('Error', 500);
    });

    if (response.statusCode == 200) {
      notifyListeners();
      return true;
    } else {
      Loader.hide();
      ToastService.showErrorToast(
          json.decode(response.body)['message'].toString());

      return false;
    }
  }


  factory Comment.fromJson(dynamic parsedJson) {
    return Comment(
      content: parsedJson['content'] ?? "",
      id: parsedJson['id'].toString(),
      replyTo: parsedJson['reply_to'].toString(),
      createdBy: parsedJson['user_id'].toString(),
      createdAt: parsedJson['created_at'] ?? "",
      postId: parsedJson['post_id'].toString(),
      username: parsedJson['username'].toString()
    );
  }
}
