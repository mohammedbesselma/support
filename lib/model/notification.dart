
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:support/model/user.dart';
import 'package:support/services/api_http.dart';
import 'package:support/utilities/comment_style.dart';

class notification with ChangeNotifier {

  String? id,user_id,post_id,content,username,createdAt;

  notification({this.id, this.user_id, this.post_id, this.content,this.username,this.createdAt});

  factory notification.fromJson(dynamic parsedJson){
    return notification(
      id: parsedJson ['id'].toString(),
      user_id: parsedJson['user_id'].toString(),
      post_id: parsedJson ['post_id'].toString(),
      content: parsedJson['content'].toString(),
      username: parsedJson['username'].toString(),
      createdAt: CommentStyle.getTextFromDate(parsedJson['created_at'].toString())
    );
  }

  Future<List<notification>> getNotification() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map<String,dynamic> json = jsonDecode(pref.getString('user').toString());
    var user = User.fromJson(json) ;
    var data = {
      'user_id': user.id ,
    };
    final response = await Network().post('getnotifications', data);
    print(response.body);
    var postObjsJson = jsonDecode(response.body)['notifications'] as List;
    List<notification> notifications =
    postObjsJson.map((tagJson) => notification.fromJson(tagJson)).toList();
    return notifications;
  }

}