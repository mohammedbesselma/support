import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:support/services/api_http.dart';
import 'package:support/services/toast_service.dart';

class User with ChangeNotifier {
  String? id;String ? userName;String? email;String? phone;
  String ? name ;
  String ? practiotionerStatus;

  User({this.id,this.name,this.email,this.phone,this.userName,this.practiotionerStatus}){
   
  }

  factory User.fromJson(Map<String,dynamic> parsedJson) {
    return new User(
      id: parsedJson['id'].toString(),
      name: parsedJson['name'].toString(),
      email: parsedJson['email'].toString(),
      phone: parsedJson['phone'].toString() ,
      practiotionerStatus: parsedJson['practitionerstatu'].toString() ,
      userName: parsedJson['username']
    );
  }
  Future<User> getUserData() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    Map<String,dynamic> json = jsonDecode(pref.getString('user').toString());
    var user = User.fromJson(json) ;
    return user;
  }
  Future<bool> updateUser(data) async {
    Network api = Network();
    http.Response response = await api.postWithHeader('update', data);
    print(response.body);
    if (response.statusCode == 200) {
      Map<String, dynamic> user = json.decode(response.body)['user'];
      SharedPreferences storage = await SharedPreferences.getInstance();
      await storage.setString('user', json.encode(user));
      notifyListeners();
      return true;
    } else {
      Loader.hide();
      ToastService.showErrorToast(
          json.decode(response.body)['message'].toString());
      return false;
    }
  }



  static Future<User> getUserById(id) async{
    var postid = {
      'user_id' : id
    };

    final  data  = await Network().post('getuser',postid);
    var postObjsJson = jsonDecode(data.body)['user'] as List;
    print (postObjsJson);
    List<User> user =
    postObjsJson.map((tagJson) => User.fromJson(tagJson)).toList();
    return user[0];
  }
}
