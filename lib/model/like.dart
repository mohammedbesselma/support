import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/cupertino.dart';
import 'package:support/services/api_http.dart';
import 'package:support/services/toast_service.dart';

class Like with ChangeNotifier {
  String? id, postId;
  String? userId;

  Like({this.id, this.postId, this.userId});

  Future<List<Like>> getLike(postId) async {
    var data = {
      'post_id': postId,
    };
    final response = await Network().post('getlike', data);
    var postObjsJson = jsonDecode(response.body)['like'] as List;
    List<Like> likes =
        postObjsJson.map((tagJson) => Like.fromJson(tagJson)).toList();
    return likes;
  }

  Future<bool> deleteLike(data) async {
    try {
      Network api = Network();
      http.Response response = await api
          .post('deletelike', data)
          .timeout(Duration(seconds: 10), onTimeout: () {
        ToastService.showErrorToast('Server error');
        return http.Response('Error', 500);
      });
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      ToastService.showErrorToast('Check your internet connection');
      return false;
    }
  }

  Future<int> createLike(data) async {
    try {
      Network api = Network();
      http.Response response = await api
          .post('createlike', data)
          .timeout(Duration(seconds: 10), onTimeout: () {
        ToastService.showErrorToast('Server error');
        return http.Response('Error', 500);
      });
      int id = jsonDecode(response.body)['like']['id'];

      if (response.statusCode == 200) {
        return id;
      } else {
        return -1;
      }
    } catch (e) {
      ToastService.showErrorToast('Check your internet connection');
      return -1;
    }
  }

  factory Like.fromJson(dynamic parsedJson) {

    return Like(
      id: parsedJson['id'].toString(),
      postId: parsedJson['post_id'].toString(),
      userId: parsedJson['user_id'].toString(),
    );
  }
}
