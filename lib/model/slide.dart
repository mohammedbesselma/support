import 'package:flutter/material.dart';

class Slide {
  final String imageUrl;
  final String title;
  final String description;

  Slide({
    required this.imageUrl,
    required this.title,
    required this.description,
  });
}

final slideList = [
  Slide(
    imageUrl: 'assets/images/firstsplash.jpg',
    title: 'Welcome!\nYour Mental Health, Is Our Concern',
    description: 'Our concern is to provide you support through difficult moment without being stigmatized',
  ),
  Slide(
    imageUrl: 'assets/images/secondsplash.jpeg',
    title: 'We Care About You And Want To Support You',
    description: 'Are you going through tough times like psychological , emotional and other traumatic issues that you cannot share with anyone, yet bothering your mind?',
  ),
  Slide(
    imageUrl: 'assets/images/thirdsplash.jpeg',
    title: 'You are one step away to let go of all your worries',
    description: 'Look no further, this platform allows you to share and receive support from others anonymously.',
  ),

];
