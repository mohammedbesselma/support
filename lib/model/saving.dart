import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/cupertino.dart';
import 'package:support/services/api_http.dart';

class Saving with ChangeNotifier {
  String? id, postId;
  String? userId;

  Saving({this.id, this.postId, this.userId});

  Future<List<Saving>> getSaving(postId) async {
    var data = {
      'post_id': postId,
    };
    final response = await Network().post('getsaving', data);
    var postObjsJson = jsonDecode(response.body)['saving'] as List;
    List<Saving> likes =
        postObjsJson.map((tagJson) => Saving.fromJson(tagJson)).toList();
    return likes;
  }

  Future<bool> deleteSaving(data) async {
    Network api = Network();

    http.Response response =
        await api.post('deletesaving', data).timeout(Duration(seconds: 10));

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }

    return false;
  }

  Future<int> createSaving(data) async {
    Network api = Network();

    http.Response response =
        await api.post('createsaving', data).timeout(Duration(seconds: 10));
    int id = jsonDecode(response.body)['saving']['id'];

    if (response.statusCode == 200) {
      return id;
    } else {
      return -1;
    }
  }

  factory Saving.fromJson(dynamic parsedJson) {
    return Saving(
      id: parsedJson['id'].toString(),
      postId: parsedJson['post_id'].toString(),
      userId: parsedJson['user_id'].toString(),
    );
  }
}
