import 'dart:convert';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:support/model/comment.dart';
import 'package:support/model/like.dart';
import 'package:support/services/api_http.dart';
import 'package:support/services/toast_service.dart';

class Post with ChangeNotifier {
  String? id;
  String? createdAt;
  String? content;
  String? userId;
  String? byParctitioner;
  String? mediaLink;
  String? postTitle;
  String ? username;
  String? categoryId;
  List<Like>? likes  = [];
  List<Comment>? comments = [];
  Post(
      {this.id,
      this.createdAt,
      this.content,
      this.userId,
      this.byParctitioner,
      this.mediaLink,
      this.postTitle,this.username,this.likes,this.comments,this.categoryId});
  Future<void> refresh ()async{
    notifyListeners();
  }

  Future<List<Post>> getPosts({idCtegory, idUser}) async {
    http.Response data;
    if (idUser != null) {
      var dat = {'id': idUser};
      data = await Network().post('getbyuser', dat);
    } else {
      var dat = {'id': idCtegory};
      if (idCtegory == 0) {
         data = await Network().get('post');
      } else
        data = await Network().post('getbycat', dat);
    }
    var postObjsJson = jsonDecode(data.body)['post'] as List;



    print (postObjsJson.toString());
    print (data.body);



    if (data.statusCode == 200) {
      List<Post> posts =
          postObjsJson.map((tagJson) => Post.fromJson(tagJson)).toList();
      return posts;
    } else {
      ToastService.showErrorToast('Somthing error');
      return [];
    }
  }
  Future <Post> getPostById(id)async {
    var postid = {
      'id' : id
    };

    final  data  = await Network().post('getpostbyid',postid);
    var postObjsJson = jsonDecode(data.body)['post'] as List;
    print (postObjsJson);
    List<Post> post =
    postObjsJson.map((tagJson) => Post.fromJson(tagJson)).toList();
    return post[0];
  }
  Future<bool> deletePost(data) async {
    try {
      Network api = Network();
      http.Response response = await api
          .post('deletepost', data)
          .timeout(Duration(seconds: 10), onTimeout: () {
        ToastService.showErrorToast('Server error');
        return http.Response('Error', 500);
      });
      if (response.statusCode == 200) {
        notifyListeners();
        return true;
      } else {
        Loader.hide();
        ToastService.showErrorToast(
            json.decode(response.body)['message'].toString());
        return false;
      }
    } catch (e) {
      ToastService.showErrorToast('Check your internet connection');
      return false;
    }
  }
  Future<bool> deleteLike(data) async {
    try {
      Network api = Network();
      http.Response response = await api
          .post('deletelike', data)
          .timeout(Duration(seconds: 10), onTimeout: () {
        ToastService.showErrorToast('Server error');
        return http.Response('Error', 500);
      });
      if (response.statusCode == 200) {

        notifyListeners();
        return true;
      } else {
        return false;
      }
    } catch (e) {
      ToastService.showErrorToast('Check your internet connection');
      return false;
    }
  }
  Future<int> createLike(data) async {
    try {
      Network api = Network();
      http.Response response = await api
          .post('createlike', data)
          .timeout(Duration(seconds: 10), onTimeout: () {
        ToastService.showErrorToast('Server error');
        return http.Response('Error', 500);
      });
      int id = jsonDecode(response.body)['like']['id'];

      if (response.statusCode == 200) {
        notifyListeners();
        return id;
      } else {
        return -1;
      }
    } catch (e) {
      ToastService.showErrorToast('Check your internet connection');
      return -1;
    }
  }

  factory Post.fromJson(dynamic parsedJson) {


      return Post(
          id: parsedJson['id'].toString(),
          createdAt: parsedJson['created_at'],
          content: parsedJson['content'],
          userId: parsedJson['user_id'].toString(),
          byParctitioner: parsedJson['by_practitioner'].toString(),
          mediaLink: parsedJson['media_link'].toString(),
          username: parsedJson['username'].toString(),
          postTitle: parsedJson['post_title'].toString(),
          categoryId: parsedJson['category_id'].toString(),
          likes: (parsedJson['likes'] as List).length != 0 ? (parsedJson['likes'] as List).map((i) => Like.fromJson(i)).toList() as List<Like> : [],
        comments: (parsedJson['comments'] as List).length != 0 ? (parsedJson['comments'] as List).map((i) => Comment.fromJson(i)).toList() as List<Comment> : []


      );



  }

  Future<bool> createPost(data, {file}) async {
    if (file != null) {
      var request = http.MultipartRequest(
          'POST', Uri.parse(Network.host + 'api/createpost'))
        ..fields.addAll(data)
        ..files.add(await http.MultipartFile.fromPath('file', file));

      var response = await request.send();

      if (response.statusCode == 200) {
        notifyListeners();
        return true;
      } else {
        return false;
      }
    } else {
      var request = http.MultipartRequest(
          'POST', Uri.parse(Network.host + 'api/createpost'))
        ..fields.addAll(data);

      var response = await request.send();
      if (response.statusCode == 200) {
        notifyListeners();
        return true;
      } else {
        return false;
      }
    }
  }
}
