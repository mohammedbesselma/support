import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:support/services/api_http.dart';

class Tip with ChangeNotifier{

  String? id,title,content;

  Tip({this.id, this.title,this.content});

  Future<List<Tip>> getTips() async {

    final response = await Network().get('tip');
    var postObjsJson = jsonDecode(response.body)['tips'] as List;
    List<Tip> tips =
    postObjsJson.map((tagJson) => Tip.fromJson(tagJson)).toList();
    return tips;
  }

  factory Tip.fromJson(dynamic parsedJson) {
    return Tip(
      content: parsedJson['content'].toString(),
      id: parsedJson['id'].toString(),
      title: parsedJson['title'].toString(),
    );
  }
}
